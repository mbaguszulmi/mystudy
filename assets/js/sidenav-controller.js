let transaction;
let animating = false;
let controlWithtimeout = false;
let success, errorDetails;
let control;
let targetPage;
let cancelHiding = false;
let cancelHidingInterval;
let animatingTimeout;

$(document).ready(() => {
    let navType = $(".sidenav").attr("navtype");
    control = (navType === 'usernav') ? 'dashboard' : 'admin';

    // init transaction for index page
    beginTransaction('/'+control+'/getpage/index');

    $("#control-nav .nav-link").click((event) => {
        clickEvent(event, control);
    });

    $("#refresh-load-btn").click((event) => {
        indirectAccess();
    });
});

let clickEvent = (event, control, removeActive = true) => {
    if (cancelHidingInterval != undefined) {
        clearInterval(cancelHidingInterval);
    }

    if (animatingTimeout != undefined) {
        clearTimeout(animatingTimeout);
        animating = false;
    }

    cancelHiding = true;

    if (transaction === undefined) {
        console.log("no-transaction");
    }
    else {
        transaction.abort();
    }

    targetPage = $(event.currentTarget).attr("target-page");
    let direct = ($(event.currentTarget).attr("direct") === '') ? false : true;

    if (removeActive) {
        $(".sidenav .nav-link.active").removeClass("active");
        $(event.currentTarget).addClass("active");
        console.log(removeActive);
    }

    // ajax access
    if (!direct) {
        indirectAccess();
    }
    // direct access
    else {
        let location = targetPage;
        // window.location = '/'+control+'/'+targetPage;
        // console.log();
        if (!$(event.currentTarget).attr("blank")) {
            window.open(location, "_self");
        }
        else {
            window.open(location, "_blank");
        }
    }
}

let indirectAccess = () => {
    let targetUrl = '/'+control+'/getpage/'+targetPage;
    normalizeAnimation();
    beginTransaction(targetUrl);

    console.log(targetPage);
}

let normalizeAnimation = () => {
    $("#fragment-content").css("display", "none");

    $("#loader").css("display", "flex");
    $("#animation-loading").removeClass("hide-animation");
    $("#animation-loading").addClass("show-animation");
    $(".sad").css("display", "none");
    $(".smile").css("display", "unset");
    // $("#error-section").css("transform", "scale(0)");
    $("#error-section").removeClass("show-error");
    // $("#error-section").addClass("hide-error");
    $("#shift-animation")[0].beginElement();
}

let beginTransaction = (targetUrl) => {
    if (!animating) {
        animating = true;
        animatingTimeout = setTimeout(() => {
            animating = false;
            if (controlWithtimeout) {
                animationControl(success, errorDetails);
            }
        }, 1000);
    }

    transaction = $.ajax({
        url: targetUrl,
        method: 'get',
        dataType: 'text',
        timeout: 60000,
        success: (data) => {
            cancelHidingInterval = setInterval(() => {
                if (!animating) {
                    cancelHiding = false;
                    clearInterval(cancelHidingInterval);
                }
            }, 1);
            
            // console.log(data);
            $("#fragment-content").html(data);

            success = true;
            if (!animating) {
                animationControl(success, errorDetails);
            }
            else {
                controlWithtimeout = true;
            }
        },
        error: (jqHXR, textStatus, errorThrown) => {
            console.log(jqHXR.status);
            console.log(textStatus);
            console.log(errorThrown);

            if (textStatus !== 'abort') {
                if (jqHXR.status == 0 || errorThrown.length == 0) {
                    errorThrown = "Unknown error";
                }
                success = false;
                errorDetails = {
                    hxrStatus: jqHXR.status,
                    textStatus: textStatus,
                    errorThrown: errorThrown
                };

                if (!animating) {
                    animationControl(success, errorDetails);
                }
                else {
                    controlWithtimeout = true;
                }
            }
        }
    });
}

let animationControl = (success, errorDetails) => {
    if (success) {
        $("#animation-loading").removeClass("show-animation");
        $("#animation-loading").addClass("hide-animation");
        setTimeout(() => {
            if (!cancelHiding) {
                $("#loader").css("display", "none");
                $("#fragment-content").css("display", "inherit");
            }
        }, 500);
    } else {
        $(".smile").css("display", "none");
        $(".sad").css("display", "unset");
        $("#error-title").html(errorDetails.textStatus + " " + errorDetails.hxrStatus);
        $("#error-details").html(errorDetails.errorThrown);
        // $("#error-section").css("transform", "scale(1)");
        // $("#error-section").removeClass("hide-error");
        $("#error-section").addClass("show-error");
        $("#shift-animation")[0].endElement();
    }
}
$(document).ready(() => {
    console.log("Work!");

    $.each($(".content .desc"), (index, element) => {
        $(element).css("grid-row", (index+1)+" / span 2");
    });

    let separator = $(".content .desc.separator");
    let len = separator.length;

    $.each(separator, (index, element) => {
        $(element).append("<div class='circle-parrent'><div class='circle-inner1'><div class='circle-inner2'></div></div></div>");

        if (index == len-1) {
            $(element).append("<div class='circle-parrent-bot'><div class='circle-inner1'><div class='circle-inner2'></div></div></div>");
        }
    });

    // $(".content .desc.separator").append("<div class='circle'><div class='circle-inner1'><div class='circle-inner2'></div></div></div>");

    $.each($(".avatar"), (index, element) => {
        $(element).css("background-image", "url('"+$(element).attr("data-image")+"')");
    });

    $("#prev").click(() => {
        let temp = $(".slider .content").last();
        // console.log(temp);
        $(temp).remove();
        
        $("#slider-content").prepend(temp);
        // console.log($(".content"));
    });

    $("#next").click(() => {
        let temp = $(".slider .content")[0];
        $($(".slider .content")[0]).remove();
        
        $("#slider-content").append(temp);
        // console.log($(".content"));
    });
});
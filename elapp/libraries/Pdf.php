<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__).'/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{
    private $htmlHeader;
    private $headerTitle;
    private $examSubject;
    private $grade;
    private $year;
    private $grades;

    public function __construct() {
        parent::__construct();

        $this->grades = array(
            1 => '1 SD',
            2 => '2 SD',
            3 => '3 SD',
            4 => '4 SD',
            5 => '5 SD',
            6 => '6 SD',
            7 => '1 SMP',
            8 => '2 SMP',
            9 => '3 SMP',
        );

        $this->headerTitle  = "Title";
        $this->examSubject  = "Subject";
        $this->grade        = "X";
        $this->year         = "2019";

        // $this->htmlHeader = '
        // <table cellpadding="5">
        //     <tr>
        //         <td width="120"><img src="/assets/images/logo.png" alt="Logo"></td>
        //         <td width="335"><h3>'.$this->headerTitle.'</h3><br/>
        //             <b>'.$this->examSubject.' grade '.$this->grade.'</b><br/>
        //             Email : info@mystudy.edu  Tel : +62 89682056995 Fax : 03410673845<br/>
        //             <a href="www.mystudy.edu">www.mystudy.edu</a> - Copyright &copy; '.$this->year.'
        //         </td>
        //     </tr>
        // </table>
        // <hr>
        // ';
        // $this->htmlHeader = 'Lorem ipsum dolor sit amet';

        $this->setHtml();
    }

    public function setHtml()
    {
        $this->htmlHeader = '
        <table cellpadding="5">
            <tr>
                <td width="120"><img src="/assets/images/logo.png" alt="Logo"></td>
                <td width="335"><h3>'.$this->headerTitle.'</h3><br/>
                    <b>'.$this->examSubject.' Grade '.$this->grade.'</b><br/>
                    Email : info@mystudy.edu  Tel : +62 89682056995 Fax : 03410673845<br/>
                    <a href="www.mystudy.edu">www.mystudy.edu</a> - Copyright &copy; '.$this->year.'
                </td>
            </tr>
        </table>
        <hr>
        ';
    }

    public function setHeaderContent($headerTitle, $examSubject, $grade, $year)
    {
        $this->headerTitle  = $headerTitle;
        $this->examSubject  = $examSubject;
        $this->grade        = $this->grades[$grade];
        $this->year         = $year;
        
        $this->setHtml();
    }

    public function Header() {
		// Logo
		// $image_file = '/assets/images/logo.png';
		// $this->Image($image_file, 10, 10, 15, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// // Set font
		// // $this->SetFont('helvetica', 'B', 20);
		// // Title
        // // $this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        
        $this->SetFont('helvetica', '', 9);
        // $this->writeHTMLCell(
        //     $w = 0, $h = 0, $x = '', $y = '',
        //     $this->htmlHeader, $border = 0, $ln = 1, $fill = 0,
        //     $reset = true, $align = 'top', $autopadding = true);
        $this->writeHTML($this->htmlHeader, true, false, true, false, '');
	}
}

<?php

class M_admin extends CI_Model
{
    public function doAuth($where)
    {
        return $this->db->get_where("admin", $where);
    }

    public function checkSession()
    {
        return $this->session->userdata("admin") !== null;
    }

    public function getMatpel($where = null) {
        // $insertData = array(
        //     'nama_matpel' => 'Ilmu Pengetahuan Sosial'
        // );
        // $this->db->insert("matpel", $insertData);
        // echo $this->db->insert_id();
        if ($where === null) {
            return $this->db->get("matpel");
        }
        else {
            return $this->db->get_where("matpel", $where);
        }
        
        // foreach ($matpel as $key => $value) {
        //     # code...
        // }
    }

    public function newMatpel($matpel)
    {
        $insertData = array(
            'nama_matpel' => $matpel
        );

        $id = 0;
        $getData = $this->db->get_where("matpel", $insertData);

        if ($getData->num_rows() <= 0) {
            $this->db->insert("matpel", $insertData);
            $id = $this->db->insert_id();
        }
        else {
            $id = $getData->result()[0]->id_matpel;
        }

        return $id;
    }

    public function addDownload($fileName)
    {
        $insertData = array(
            'nama_file' => $fileName
        );

        $this->db->insert("download", $insertData);
        return $this->db->insert_id();
    }

    public function addCourseData($insertData)
    {
        $this->db->insert("materi", $insertData);
        return $this->db->affected_rows();
    }

    public function updateCourseData($updateData, $id)
    {
        $this->db->set($updateData);
        $this->db->where('id_materi', $id);
        $this->db->update("materi");
        return $this->db->affected_rows();
    }

    public function getCourse($where = null)
    {
        if ($where !== null) {
            return $this->db->get_where("v_materi", $where);
        }
        else {
            return $this->db->get("v_materi");
        }
    }

    public function getCourseData($where)
    {
        return $this->db->get_where("materi", $where);
    }

    public function setExam($insertData)
    {
        $this->db->insert("soal", $insertData);
        return $this->db->insert_id();
    }

    public function setExamQuestion($insertData)
    {
        $this->db->insert("pertanyaan", $insertData);
        return $this->db->insert_id();
    }

    public function setExamAnswer($insertData)
    {
        $this->db->insert("pilihan", $insertData);
        return $this->db->insert_id();
    }

    public function getExam($where = null) 
    {
        if ($where === null) {
            return $this->db->get("v_soal");
        } else {
            return $this->db->get_where("v_soal", $where);
        }
    }

    public function getExamData($where)
    {
        return $this->db->get_where("soal", $where);
    }

    public function updateExamData($updateData, $id)
    {
        $this->db->set($updateData);
        $this->db->where("id_soal", $id);
        $this->db->update("soal");
        return $this->db->affected_rows();
    }

    public function getQuestionData($where)
    {
        return $this->db->get_where("pertanyaan", $where);
    }

    public function updateQuestionData($updateData, $id)
    {
        $this->db->set($updateData);
        $this->db->where("id_pertanyaan", $id);
        $this->db->update("pertanyaan");
        return $this->db->affected_rows();
    }

    public function getAnswerData($where)
    {
        return $this->db->get_where("pilihan", $where);
    }

    public function updateAnswerData($updateData, $id)
    {
        $this->db->set($updateData);
        $this->db->where("id_pilihan", $id);
        $this->db->update("pilihan");
        return $this->db->affected_rows();
    }

    public function getRelation($where)
    {
        return $this->db->get_where("relasi_soal_materi", $where);
    }

    public function setRelation($insertData)
    {
        $this->db->insert("relasi_soal_materi", $insertData);
        return $this->db->affected_rows();
    }

    public function updateRelation($updateData, $id) 
    {
        $this->db->set($updateData);
        $this->db->where('id_materi', $id);
        $this->db->update("relasi_soal_materi");
        return $this->db->affected_rows();
    }

    public function getRankElementary($where = null)
    {
        if ($where === null) {
            return $this->db->get("v_rank_sd");
        }
        else {
            return $this->db->get_where("v_rank_sd", $where);
        }
    }

    public function getRankJunior($where = null)
    {
        if ($where === null) {
            return $this->db->get("v_rank_smp");
        }
        else {
            return $this->db->get_where("v_rank_smp", $where);
        }
    }

    public function addChat($insertData)
    {
        $this->db->insert("chatting", $insertData);
        return $this->db->affected_rows();
    }

    public function getChat()
    {
        return $this->db->get("v_chatting");
    }

    public function getStudent()
    {
        return $this->db->get("siswa");
    }
}

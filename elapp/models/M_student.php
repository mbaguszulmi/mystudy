<?php

class M_student extends CI_Model
{
    public function doAuth($where)
    {
        return $this->db->get_where("siswa", $where);
    }

    public function checkSession()
    {
        return $this->session->userdata("student") !== null;
    }

    public function getCourse($where)
    {
        return $this->db->get_where("v_materi", $where);
    }

    public function getReadCourseData($where)
    {
        return $this->db->get_where("baca_materi", $where);
    }

    public function addReadCourse($insertData)
    {
        $this->db->insert("baca_materi", $insertData);
        return $this->db->affected_rows();
    }

    public function getReadCourse($where = null)
    {
        if ($where === null) {
            return $this->db->get("baca_materi");
        }
        else {
            return $this->db->get_where("baca_materi", $where);
        }
    }

    public function getRank($grade, $limit = 0)
    {
        if ($limit != 0) {
            $this->db->limit($limit);
        }

        if ($grade >= 1 && $grade <= 6) {
            $tableName = "v_rank_sd";
        }
        else {
            $tableName = "v_rank_smp";
        }

        return $this->db->get($tableName);
    }

    public function getExam($where, $notIn = null)
    {
        if ($notIn !== null) {
            if (count($notIn) > 0) {
                $this->db->where_not_in("id_soal", $notIn);
            }
        }
        return $this->db->get_where("v_soal", $where);
    }

    public function getExamData($where)
    {
        return $this->db->get_where("soal", $where);
    }

    public function getQuestionData($where)
    {
        return $this->db->get_where("pertanyaan", $where);
    }

    public function getAnswerData($where, $noAnswer = false)
    {
        if ($noAnswer) {
            $this->db->select('id_pilihan, id_pertanyaan, jawaban');
        }
        return $this->db->get_where("pilihan", $where);
    }

    public function insertExamResult($insertData)
    {
        $this->db->insert('ujian', $insertData);
        return $this->db->affected_rows();
    }

    public function getUjian($where, $onlyId = false)
    {
        if ($onlyId) {
            $this->db->select('id_soal');
        }

        return $this->db->get_where("v_ujian", $where);
    }

    public function addChat($insertData)
    {
        $this->db->insert("chatting", $insertData);
        return $this->db->affected_rows();
    }

    public function getChat()
    {
        return $this->db->get("v_chatting");
    }

    public function addStudent($insertData)
    {
        $this->db->insert("siswa", $insertData);
        return $this->db->affected_rows();
    }

    public function updateStudent($updateData, $id)
    {
        $this->db->set($updateData);
        $this->db->where("id_siswa", $id);
        $this->db->update("siswa");
        return $this->db->affected_rows();
    }
}

<?php

// Used for contents of side menu.
// There's two menu configurations, that is for user and admin.
// Both configurations have same structure. First 'navtype' describes
// who use this menu. And it will used for identifying the controller used
// for target url.
//
// There's two sections of menu, top and bottom. This will use separation
// for these two sections.
// Each of both sections, there's menu elements that contains icon, name,
// target-page, and direct.
// icon defines the icon used for current elements.
// name defines the name for current elements.
// target-page defines the page we want to navigate.
// direct indicates this url can be direct access or not. If not, then
// the system will access target url with ajax.
//
// For icon name can be found at this link https://fontawesome.com/icons?d=gallery&m=free
// 
// MBZ

$config['menu-user'] = array(
    'navtype' => 'usernav',
    'top' => array(
        array(
            'icon' => 'tachometer-alt',
            'name' => 'Dashboard',
            'target-page' => 'index',
            'direct' => false
        ),
        array(
            'icon' => 'book',
            'name' => 'Course',
            'target-page' => 'course',
            'direct' => false
        ),
        array(
            'icon' => 'tasks',
            'name' => 'Exam',
            'target-page' => 'exam',
            'direct' => false
        ),
        array(
            'icon' => 'chart-bar',
            'name' => 'View Rank',
            'target-page' => 'rank',
            'direct' => false
        ),
        array(
            'icon' => 'envelope',
            'name' => 'Messages',
            'target-page' => 'message',
            'direct' => false
        ),
        array(
            'icon' => 'cogs',
            'name' => 'Settings',
            'target-page' => 'settings',
            'direct' => false
        )
    ),
    'bottom' => array(
        array(
            'icon' => 'sign-out-alt',
            'name' => 'Sign Out',
            'target-page' => '/dashboard/logout',
            'direct' => true
        )
    )
);

$config['menu-admin'] = array(
    'navtype' => 'adminnav',
    'top' => array(
        array(
            'icon' => 'tachometer-alt',
            'name' => 'Dashboard',
            'target-page' => 'index',
            'direct' => false
        ),
        array(
            'icon' => 'book',
            'name' => 'Upload Course',
            'target-page' => 'course',
            'direct' => false
        ),
        array(
            'icon' => 'tasks',
            'name' => 'Make Exam',
            'target-page' => 'exam',
            'direct' => false
        ),
        array(
            'icon' => 'chart-bar',
            'name' => 'View Rank',
            'target-page' => 'rank',
            'direct' => false
        ),
        array(
            'icon' => 'envelope',
            'name' => 'Messages',
            'target-page' => 'message',
            'direct' => false
        )
    ),
    'bottom' => array(
        array(
            'icon' => 'sign-out-alt',
            'name' => 'Sign Out',
            'target-page' => '/admin/logout',
            'direct' => true
        )
    )
);
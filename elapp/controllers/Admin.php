<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    private $siteconf;
    private $menu;

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_admin');
        $this->siteconf = $this->config->item('site');

        $this->config->load('menu');
        $this->menu = $this->config->item('menu-admin');
    }

    public function index()
    {
        if (!$this->m_admin->checkSession()) {
            redirect(base_url("admin/login"));
        }

        $data['title'] = "Administrator";
        $data['page'] = "dashboard";
        $data['site'] = $this->siteconf;
        $data['menu'] = $this->menu;
        $data['sessionData'] = $this->session->userdata();

        $this->load->view('template/head', $data);
        $this->load->view('template/topnav');
        $this->load->view('template/dashboardcontent');
        $this->load->view('template/foot');
    }

    public function getPage($pageName, $id = null) {
        // if (!file_exists(APPPATH.'views/'.$pageName.'.php')) {
        //     show_404();
        // }
        
        $data['gen'] = true;

        if (!file_exists(APPPATH.'views/admin/'.$pageName.'.php')) {
            show_404();
        }

        if ($pageName == 'index') {
            $data['numStudent'] = $this->m_admin->getStudent()->num_rows();
            $data['numCourse'] = $this->m_admin->getCourse()->num_rows();
            $data['numExam'] = $this->m_admin->getExam()->num_rows();
        }
        else if ($pageName == 'newcourse' || $pageName == 'newexam') {
            $data['matpelList'] = $this->m_admin->getMatpel()->result();
        }
        else if ($pageName == 'course') {
            $data['courseList'] = $this->m_admin->getCourse()->result();
        }
        else if ($pageName == 'exam') {
            $data['examList'] = $this->m_admin->getExam()->result();
        }
        else if ($pageName == 'editcourse') {
            $where = array(
                'id_materi' => $id
            );
            $data['courseData'] = $this->m_admin->getCourseData($where)->result()[0];
            $data['matpelList'] = $this->m_admin->getMatpel()->result();
        }
        else if ($pageName == 'editexam' || $pageName == 'exampdf') {
            $where = array(
                'id_soal' => $id
            );

            if ($pageName == 'editexam') {
                $data['examData'] = $this->m_admin->getExamData($where)->result()[0];
                $data['matpelList'] = $this->m_admin->getMatpel()->result();
            } else {
                $this->load->library('Pdf');
                $data['examData'] = $this->m_admin->getExam($where)->result()[0];
            }
            

            $data['questionData'] = $this->m_admin->getQuestionData($where)->result();

            $data['answerData'] = array();
            foreach ($data['questionData'] as $key => $value) {
                $where = array(
                    'id_pertanyaan' => $value->id_pertanyaan
                );

                $data['answerData'] = array_merge($data['answerData'], $this->m_admin->getAnswerData($where)->result());
            }

            $data['numAns'] = count($data['answerData'])/count($data['questionData']);
        }
        else if ($pageName == 'relate') {
            $where = array(
                'id_materi' => $id
            );
            $data['relateData'] = $this->m_admin->getRelation($where)->result();
            $data['numRelate'] = count($data['relateData']);
            if ($data['numRelate'] > 0) {
                $data['relateData'] = $data['relateData'][0];
            }
            $data['courseData'] = $this->m_admin->getCourseData($where)->result()[0];

            $where = array(
                'id_matpel' => $data['courseData']->id_matpel
            );
            $data['matpel'] = $this->m_admin->getMatpel($where)->result()[0];

            $data['examList'] = $this->m_admin->getExamData($where)->result();
        }
        else if ($pageName == 'rank') {
            $data['elementaryRank'] = $this->m_admin->getRankElementary()->result();
            $data['juniorRank'] = $this->m_admin->getRankJunior()->result();
        }
        elseif ($pageName == 'message') {
            $chat['isAdmin'] = true;
            $chat['title'] = 'Public';
            $chat['idSender'] = $this->session->userdata("admin_id");;
            $chat['name'] = '<i class="fas fa-crown fa-fw"></i> Administrator';
            $chat['chatData'] = $this->m_admin->getChat()->result();
            $this->load->view('template/chatroom', $chat);
        }

        $this->load->view('admin/'.$pageName, $data);
    }

    public function submitMessage()
    {
        $id = $this->input->post("id");
        $message = $this->input->post("message");
        $sender = 1;

        $insertData = array(
            'id_sender' => $id,
            'pesan'     => $message,
            'sender'    => $sender
        );

        $affRows = $this->m_admin->addChat($insertData);

        $result = array(
            'success' => true,
            'message' => 'Sending chat success'
        );
        if ($affRows <= 0) {
            $result = array(
                'success' => true,
                'message' => 'Sending chat success'
            );
        }

        echo json_encode($result);
    }

    public function addRelate()
    {
        $idMateri   = $this->input->post("id_materi");
        $idSoal     = $this->input->post("id_soal");

        $insertData = array(
            'id_materi' => $idMateri,
            'id_soal'   => $idSoal
        );

        $affRows = $this->m_admin->setRelation($insertData);
        if ($affRows > 0) {
            $result['success'] = true;
            $result['message'] = 'Success';
        }
        else {
            $result['success'] = false;
            $result['message'] = 'DB insert error!';
        }

        echo json_encode($result);
    }

    public function updateRelate()
    {
        $idMateri   = $this->input->post("id_materi");
        $idSoal     = $this->input->post("id_soal");

        $updateData = array(
            'id_soal' => $idSoal
        );

        $affRows = $this->m_admin->updateRelation($updateData, $idMateri);
        if ($affRows > 0) {
            $result['success'] = true;
            $result['message'] = 'Success';
        }
        else if ($affRows < 0) {
            $result['success'] = false;
            $result['message'] = 'DB insert error!';
        }

        echo json_encode($result);
    }

    public function addCourse()
    {
        $namaMateri     = $this->input->post("nama_materi");
        $kelas          = $this->input->post("kelas");
        $idMatpel       = $this->input->post("id_matpel");
        if(!$idMatpel) {
            $namaMatpel = $this->input->post("nama_matpel");
            $idMatpel   = $this->m_admin->newMatpel($namaMatpel);
        }
        $fileName       = substr(str_replace(" ", "-", $namaMateri), 0, 15)."-".date('ymdhis').".pdf";
        $idUser         = $this->session->userdata("admin_id");
        $idDownload     = 0;

        // file upload settings
            $config['upload_path']          = './uploads/pdf/';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 8192;
            $config['file_name']            = $fileName;
        // end

        $this->load->library('upload', $config);

        $result['success'] = true;
        $result['message'] = 'Success';
        if (!$this->upload->do_upload('file_materi')) {
            $result['success'] = false;
            $result['message'] = $this->upload->display_errors();
        }
        else {
            $idDownload = $this->m_admin->addDownload($fileName);
            // $data = array('upload_data' => $this->upload->data());
            // echo "<pre>";
            // print_r($data);

            $insertData = array(
                'id_user'       => $idUser,
                'kelas'         => $kelas,
                'id_matpel'     => $idMatpel,
                'nama_materi'   => $namaMateri,
                'id_download'   => $idDownload,
            );
            $affRows = $this->m_admin->addCourseData($insertData);

            if ($affRows <= 0) {
                $result['success'] = false;
                $result['message'] = 'DB Insert error!';
            }
        }

        echo json_encode($result);
    }

    public function updateCourse()
    {
        $idMateri      = $this->input->post("id_materi");
        $namaMateri     = $this->input->post("nama_materi");
        $kelas          = $this->input->post("kelas");
        $idMatpel       = $this->input->post("id_matpel");
        if(!$idMatpel) {
            $namaMatpel = $this->input->post("nama_matpel");
            $idMatpel   = $this->m_admin->newMatpel($namaMatpel);
        }
        $fileName       = substr(str_replace(" ", "-", $namaMateri), 0, 15)."-".date('ymdhis').".pdf";
        $idUser         = $this->session->userdata("admin_id");
        $idDownload     = $this->input->post("id_download");

        $result['success'] = true;
        $result['message'] = 'Success';
        $result['test'] = $this->input->post("update_file");
        if ($this->input->post("update_file")) {
            // file upload settings
                $config['upload_path']          = './uploads/pdf/';
                $config['allowed_types']        = 'pdf';
                $config['max_size']             = 8192;
                $config['file_name']            = $fileName;
            // end

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_materi')) {
                $result['success'] = false;
                $result['message'] = $this->upload->display_errors();
            }
            else {
                $idDownload = $this->m_admin->addDownload($fileName);
            }
        }

        if ($result['success'] !== false) {
            $updateData = array(
                // 'id_materi'     => $idMateri,
                'id_user'       => $idUser,
                'kelas'         => $kelas,
                'id_matpel'     => $idMatpel,
                'nama_materi'   => $namaMateri,
                'id_download'   => $idDownload,
            );

            $affRows = $this->m_admin->updateCourseData($updateData, $idMateri);

            if ($affRows < 0) {
                $result['success'] = false;
                $result['message'] = 'DB update error!';
            }
        }

        echo json_encode($result);
    }

    public function addExam()
    {
        $namaSoal       = $this->input->post("nama_soal");
        $kelas          = $this->input->post("kelas");
        $idMatpel       = $this->input->post("id_matpel");
        if (!$idMatpel) {
            $namaMatpel = $this->input->post("nama_matpel");
            $idMatpel   = $this->m_admin->newMatpel($namaMatpel);
        }
        $waktuMenit     = $this->input->post("waktu_menit");
        $pertanyaan     = $this->input->post("pertanyaan");
        $benar          = $this->input->post("benar");
        $jawaban        = $this->input->post("jawaban");
        $numQuest       = count($pertanyaan);
        $numAns         = count($jawaban)/$numQuest;
        $idUser         = $this->session->userdata("admin_id");
        $ansCount       = 0;

        // test
            // echo "<pre>";
            // echo "Nama soal : ".$namaSoal;
            // echo "Kelas : ".$kelas;
            // echo "Id matpel : ".$idMatpel;
            // echo "Waktu menit : ".$waktuMenit;
            // print_r($pertanyaan);
            // print_r($benar);
            // print_r($jawaban);
            // echo count($pertanyaan);
            // echo count($benar);
            // echo count($jawaban);
            // echo "</pre>";
        // endtest

        $success = true;

        $dataSoal = array(
            'id_user'       => $idUser,
            'kelas'         => $kelas,
            'id_matpel'     => $idMatpel,
            'nama_soal'     => $namaSoal,
            'waktu_menit'   => $waktuMenit
        );

        $idSoal = $this->m_admin->setExam($dataSoal);

        if ($idSoal > 0) {
            for ($i=0; $i < $numQuest; $i++) { 
                $dataPertanyaan = array(
                    'id_soal'       => $idSoal,
                    'pertanyaan'    => $pertanyaan[$i]
                );
    
                $idPertanyaan = $this->m_admin->setExamQuestion($dataPertanyaan);
    
                if ($idPertanyaan > 0) {
                    for ($j=0; $j < $numAns; $j++) { 
                        $isTrue = false;
                        if ((int)$benar[$i] == $ansCount) {
                            $isTrue = true;
                        }
                        $dataJawaban = array(
                            'id_pertanyaan' => $idPertanyaan,
                            'jawaban'       => $jawaban[$ansCount++],
                            'benar'         => $isTrue
                        );
        
                        $idJawaban = $this->m_admin->setExamAnswer($dataJawaban);

                        if ($idJawaban <= 0) {
                            $success = false;
                            break;
                        }
                    }
                }
                else {
                    $success = false;
                    break;
                }
            }
        }
        else {
            $success = false;
        }

        $result = array(
            'success' => $success
        );

        echo json_encode($result);
    }

    public function updateExam()
    {
        $idSoal         = $this->input->post("id_soal");
        $namaSoal       = $this->input->post("nama_soal");
        $kelas          = $this->input->post("kelas");
        $idMatpel       = $this->input->post("id_matpel");
        if (!$idMatpel) {
            $namaMatpel = $this->input->post("nama_matpel");
            $idMatpel   = $this->m_admin->newMatpel($namaMatpel);
        }
        $waktuMenit     = $this->input->post("waktu_menit");
        $pertanyaan     = $this->input->post("pertanyaan");
        $benar          = $this->input->post("benar");
        $jawaban        = $this->input->post("jawaban");
        $numQuest       = count($pertanyaan);
        $numAns         = count($jawaban)/$numQuest;
        $idUser         = $this->session->userdata("admin_id");

        // test
            // echo "<pre>";
            // echo "Nama soal : ".$namaSoal."<br/>";
            // echo "Kelas : ".$kelas."<br/>";
            // echo "Id matpel : ".$idMatpel."<br/>";
            // echo "Waktu menit : ".$waktuMenit."<br/>";
            // print_r($pertanyaan);
            // print_r($benar);
            // print_r($jawaban);
            // echo count($pertanyaan)."<br/>";
            // echo count($benar)."<br/>";
            // echo count($jawaban)."<br/>";
            // echo "</pre>";
        // endtest

        $result['success'] = true;
        $result['message'] = 'Success';

        $updateData = array(
            'nama_soal' => $namaSoal,
            'id_matpel' => $idMatpel,
            'kelas'     => $kelas,
            'id_user'   => $idUser,
            'waktu_menit'   => $waktuMenit
        );

        $affRows = $this->m_admin->updateExamData($updateData, $idSoal);
        if ($affRows >= 0) {
            foreach ($pertanyaan as $key => $value) {
                $updateData = array(
                    'pertanyaan' => $value
                );

                $affRows = $this->m_admin->updateQuestionData($updateData, $key);
                if ($affRows < 0) {
                    $result['success'] = false;
                    $result['message'] = 'Update question data failed';
                    break;
                }
            }

            if ($result['success']) {
                $i = 0;
                $j = 0;
                foreach ($jawaban as $key => $value) {
                    $isTrue = false;
                    if ((int)$benar[$i] == $key) {
                        $isTrue = true;
                    }
                    $updateData = array(
                        'jawaban'   => $value,
                        'benar'     => $isTrue
                    );
                    $affRows = $this->m_admin->updateAnswerData($updateData, $key);
                    if ($affRows < 0) {
                        $result['success'] = false;
                        $result['message'] = 'Update answer data failed';
                        break;
                    }

                    $j++;
                    if ($j == $numAns) {
                        $i++;
                        $j = 0;
                    }
                }
            }
        }
        else {
            $result['success'] = false;
            $result['message'] = 'Update exam data failed';
        }

        echo json_encode($result);
    }

    public function test() {
        $this->m_admin->newMatpel('Matematika');
    }

    public function login() {
        $error = $this->input->get("status");
        if ($error) {
            $data['error_message'] = 'Make sure username and password are correct.';
        }

        if ($this->m_admin->checkSession()) {
            redirect(base_url("admin"));
        }

        $data['title'] = 'Administrator Login';
        $data['page'] = "adminlogin";
        $data['site'] = $this->siteconf;
        $data['menu'] = $this->menu;
        $data['processUrl'] = base_url("admin/processlogin");;
        $data['isStudent'] = false;

        $this->load->view('template/head', $data);
        if ($error) {
            $this->load->view('template/error-popup');
        }
        $this->load->view('template/login');
        $this->load->view('template/foot');
    }

    public function processLogin()
    {
        $username = $this->input->post('username');
        $password = sha1($this->input->post('password'));
        $remember = ($this->input->post('remember')) ? true : false;

        $where = array(
            'username' => $username,
            'password' => $password
        );

        $authRespons = $this->m_admin->doAuth($where);
        $authNumRows = $authRespons->num_rows();

        if ($authNumRows > 0) {
            $sessionData = array(
                'name'              => $username,
                'admin'             => $username,
                'admin_password'    => $password,
                'admin_id'          => $authRespons->result()[0]->id_user
            );

            $this->session->set_userdata($sessionData);
            
            if ($remember) {
                // cookie login
            }
            
            echo "Redirecting to dashboard...";
            redirect(base_url("admin"));
        }
        else {
            echo "Login fail. Redirecting to login page...";
            redirect(base_url("admin/login?status=-1"));
        }

        // echo $this->m_admin->checkSession();
        // echo "<a href='logout'>destroy</a>";

        // echo $username.'<br>';
        // echo $password.'<br>';
        // echo $remember.'<br>';
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url("admin/login"));
    }
}

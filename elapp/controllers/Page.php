<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller
{
    private $siteconf;

    function __construct()
    {
        parent::__construct();
        $this->siteconf = $this->config->item('site');
    }

    public function index()
    {
        $data['title'] = $this->siteconf['name'];
        $data['page'] = "index";
        $data['site'] = $this->siteconf;
        $data['sessionData'] = $this->session->userdata();

        $this->load->view('template/head', $data);
        $this->load->view('template/topnav');
        $this->load->view('template/header');
        $this->load->view('page/indexpage');
        $this->load->view('template/footer');
        $this->load->view('template/foot');
    }

    public function about()
    {
        $data['title'] = "About Us";
        $data['page'] = "about";
        $data['site'] = $this->siteconf;
        $data['sessionData'] = $this->session->userdata();

        $this->load->view('template/head', $data);
        $this->load->view('template/topnav');
        $this->load->view('page/about', $data);
        $this->load->view('template/footer');
        $this->load->view('template/foot', $data);
    }

    public function faq()
    {
        $data['title'] = "FAQ";
        $data['page'] = "faq";
        $data['site'] = $this->siteconf;
        $data['sessionData'] = $this->session->userdata();

        $this->load->view('template/head', $data);
        $this->load->view('template/topnav');
        $this->load->view('page/faq', $data);
        $this->load->view('template/footer');
        $this->load->view('template/foot', $data);
    }

    public function contact()
    {
        $data['title'] = "Contact";
        $data['page'] = "contact";
        $data['site'] = $this->siteconf;
        $data['sessionData'] = $this->session->userdata();

        $this->load->view('template/head', $data);
        $this->load->view('template/topnav');
        $this->load->view('page/contact', $data);
        $this->load->view('template/footer');
        $this->load->view('template/foot', $data);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    private $siteconf;
    private $menu;
    private $userData;

    function __construct()
    {
        parent::__construct();
        $this->load->model("m_student");
        $this->siteconf = $this->config->item('site');

        $this->config->load('menu');
        $this->menu = $this->config->item('menu-user');

        $where = array(
            'email' => $this->session->userdata("student"),
        );
        $userData = $this->m_student->doAuth($where);

        if ($userData->num_rows() > 0) {
            $this->userData = $this->m_student->doAuth($where)->result()[0];
        }
    }

    public function index()
    {
        if (!$this->m_student->checkSession()) {
            redirect(base_url("dashboard/login"));
        }

        $data['title'] = "Dashboard";
        $data['page'] = "dashboard";
        $data['site'] = $this->siteconf;
        $data['menu'] = $this->menu;
        $data['sessionData'] = $this->session->userdata();

        $this->load->view('template/head', $data);
        $this->load->view('template/topnav');
        $this->load->view('template/dashboardcontent');
        $this->load->view('template/foot');
    }

    public function getPage($pageName, $id = null) {
        // if (!file_exists(APPPATH.'views/'.$pageName.'.php')) {
        //     show_404();
        // }
        if (!file_exists(APPPATH.'views/dashboard/'.$pageName.'.php')) {
            show_404();
        }

        $data['gen'] = true;

        if ($pageName == 'index') {
            $data['rank'] = "Nan";
            
            $rankAll = $this->m_student->getRank((int)$this->userData->kelas, 0)->result();

            $i = 1;
            foreach ($rankAll as $key => $value) {
                if ($value->id_siswa == $this->userData->id_siswa) {
                    $data['rank'] = $i;
                    break;
                }

                $i++;
            }

            $where = array(
                'id_siswa' => $this->userData->id_siswa
            );
            $data['numRead'] = $this->m_student->getReadCourse($where)->num_rows();

            $data['numExam'] = $this->m_student->getUjian($where)->num_rows();
        }
        else if ($pageName == 'course') {
            $where = array(
                'kelas' => $this->userData->kelas
            );
            $data['courseList'] = $this->m_student->getCourse($where)->result();
        }
        else if ($pageName == 'rank') {
            $data['rankData'] = $this->m_student->getRank((int)$this->userData->kelas, 10)->result();
            $data['rank'] = "Nan";
            
            $i = 1;
            foreach ($data['rankData'] as $key => $value) {
                if ($value->id_siswa == $this->userData->id_siswa) {
                    $data['rank'] = $i;
                    break;
                }

                $i++;
            }

            if ($data['rank'] == "Nan") {
                $rankAll = $this->m_student->getRank((int)$this->userData->kelas, 0)->result();

                $i = 1;
                foreach ($rankAll as $key => $value) {
                    if ($value->id_siswa == $this->userData->id_siswa) {
                        $data['rank'] = $i;
                        break;
                    }

                    $i++;
                }
            }
        }
        else if ($pageName == 'exam') {
            $where = array(
                'id_siswa'  => $this->userData->id_siswa
            );
    
            $data['examResult'] = $this->m_student->getUjian($where)->result();

            $notIn = $this->m_student->getUjian($where, true)->result_array();
            $notIn = array_unique(array_map('current', $notIn));

            $where = array(
                'kelas' => $this->userData->kelas
            );
            $data['examList'] = $this->m_student->getExam($where, $notIn)->result();
        }
        else if ($pageName == 'examroom') {
            $where = array(
                'id_soal' => $id
            );

            $data['examData'] = $this->m_student->getExam($where);
            if ($data['examData']->num_rows() <= 0) {
                show_404();
            }
            else {
                $data['examData'] = $data['examData']->result_array()[0];
            }

            $data['questionData'] = $this->m_student->getQuestionData($where)->result_array();

            $data['answerData'] = array();
            foreach ($data['questionData'] as $key => $value) {
                $where = array(
                    'id_pertanyaan' => $value['id_pertanyaan']
                );

                $data['answerData'] = array_merge($data['answerData'], $this->m_student->getAnswerData($where, true)->result_array());
            }

            $data['numAns'] = count($data['answerData'])/count($data['questionData']);
        }
        else if ($pageName == 'exampdf') {
            $this->load->library('Pdf');

            // block if student have not finish this exam
            $where = array(
                'id_siswa'  => $this->userData->id_siswa,
                'id_soal'   => $id
            );
    
            $numResult = $this->m_student->getUjian($where)->num_rows();
            if ($numResult <= 0) {
                show_404();
            }

            $where = array(
                'id_soal' => $id
            );
            $data['examData'] = $this->m_student->getExam($where)->result()[0];
            $data['questionData'] = $this->m_student->getQuestionData($where)->result();

            $data['answerData'] = array();
            foreach ($data['questionData'] as $key => $value) {
                $where = array(
                    'id_pertanyaan' => $value->id_pertanyaan
                );

                $data['answerData'] = array_merge($data['answerData'], $this->m_student->getAnswerData($where)->result());
            }

            $data['numAns'] = count($data['answerData'])/count($data['questionData']);
        }
        else if ($pageName == 'message') {
            $chat['isAdmin'] = false;
            $chat['title'] = 'Public';
            $chat['idSender'] = $this->userData->id_siswa;
            $chat['name'] = $this->userData->nama;
            $chat['chatData'] = $this->m_student->getChat()->result();
            $this->load->view('template/chatroom', $chat);
        }
        else if ($pageName == 'settings') {
            $data['curPass'] = $this->userData->password;
            $data['userData'] = $this->userData;
        }

        $this->load->view('dashboard/'.$pageName, $data);
    }

    public function submitMessage()
    {
        $id = $this->input->post("id");
        $message = $this->input->post("message");
        $sender = 0;

        $insertData = array(
            'id_sender' => $id,
            'pesan'     => $message,
            'sender'    => $sender
        );

        $affRows = $this->m_student->addChat($insertData);

        $result = array(
            'success' => true,
            'message' => 'Sending chat success'
        );
        if ($affRows <= 0) {
            $result = array(
                'success' => true,
                'message' => 'Sending chat success'
            );
        }

        echo json_encode($result);
    }

    public function test()
    {
        $where = array(
            'id_siswa'  => $this->userData->id_siswa
        );

        $arr = $this->m_student->getUjian($where, true)->result_array();
        // $arr = array(
        //     array(
        //         'id_soal' => 2
        //     ),
        //     array(
        //         'id_soal' => 3
        //     )
        // );

        echo '<pre>';
        print_r($arr);
        print_r(array_unique(array_map('current', $arr)));
        // print_r(array_reduce($arr, 'array_merge', []));
        echo '</pre>';
    }

    public function submitAnswer()
    {
        $answerData = $this->input->post("answerData");
        $idSoal = $this->input->post("id_soal");

        $userAnswer = array();
        foreach ($answerData as $key => $value) {
            array_push($userAnswer, $value['answerId']);
        }

        $where = array(
            'id_soal' => $idSoal
        );

        $questionData = $this->m_student->getQuestionData($where)->result();

        $correctAnswer = array();
        foreach ($questionData as $key => $value) {
            $where = array(
                'id_pertanyaan' => $value->id_pertanyaan,
                'benar'         => 1
            );

            $answerData = $this->m_student->getAnswerData($where)->result();
            array_push($correctAnswer, $answerData[0]->id_pilihan);
        }

        $intersectAnswer = array_intersect($userAnswer, $correctAnswer);
        $numCorrect = count($intersectAnswer);
        $numTotal   = count($correctAnswer);
        $points = ($numCorrect*100)/$numTotal;

        // begin test
            // echo '<pre>';
            // print_r($answerData);
            // echo $idSoal;
            // echo '</pre>';
            // print_r($userAnswer);
            // print_r($correctAnswer);
            // print_r($points);
        // end test

        $insertData = array(
            'id_soal'   => $idSoal,
            'id_siswa'  => $this->userData->id_siswa,
            'poin'      => $points,
            'benar'     => $numCorrect,
            'salah'     => $numTotal - $numCorrect
        );

        $affRows = $this->m_student->insertExamResult($insertData);
        if ($affRows > 0) {
            $result['success'] = true;
            $result['message'] = "Sending exam value success";
            $result['examResult'] = array(
                'points'        => $points,
                'trueAnswer'    => $numCorrect,
                'falseAnswer'   => $numTotal - $numCorrect
            );
        }
        else {
            $result['success'] = false;
            $result['message'] = "Sending exam value failed";
        }

        echo json_encode($result);
    }

    public function readCourse($id)
    {
        $where = array(
            'id_materi' => $id
        );

        $courseData = $this->m_student->getCourse($where);
        if ($courseData->num_rows() <= 0) {
            show_404();
        }
        else {
            $namaFile = $courseData->result()[0]->nama_file;

            $where = array(
                'id_materi' => $id,
                'id_siswa'  => $this->userData->id_siswa
            );
            $readNumRows = $this->m_student->getReadCourseData($where)->num_rows();

            $affRows = 1;
            if ($readNumRows <= 0) {
                $insertData = $where;
                $affRows = $this->m_student->addReadCourse($insertData);    
            }

            if ($affRows > 0) {
                redirect(base_url("pdfviewer?file=").base_url("uploads/pdf/$namaFile"));
            }
        }
    }

    public function login()
    {
        $error = $this->input->get("status");
        if ($error) {
            $data['error_message'] = 'Make sure email and password are correct.';
        }

        if ($this->m_student->checkSession()) {
            redirect(base_url("dashboard"));
        }

        $data['title'] = 'Student Login';
        $data['page'] = "studentlogin";
        $data['site'] = $this->siteconf;
        $data['menu'] = $this->menu;
        $data['processUrl'] = base_url("dashboard/processlogin");
        $data['isStudent'] = true;

        $this->load->view('template/head', $data);
        if ($error) {
            $this->load->view('template/error-popup');
        }
        $this->load->view('template/login');
        $this->load->view('template/foot');
    }

    public function processLogin($where = null)
    {
        if ($where === null) {
            $email = $this->input->post('email');
            $password = sha1($this->input->post('password'));
            $remember = ($this->input->post('remember')) ? true : false;
    
            $where = array(
                'email' => $email,
                'password' => $password,
                'aktif' => 1
            );
        }
        else {
            $email    = $where['email'];
            $password = $where['password'];
            $remember = false;
        }

        $authRespons = $this->m_student->doAuth($where);
        $authNumRows = $authRespons->num_rows();

        if ($authNumRows > 0) {
            $this->userData = $authRespons->result()[0];

            $sessionData = array(
                'name'  => $authRespons->result()[0]->nama,
                'student' => $email,
                'password' => $password
            );

            $this->session->set_userdata($sessionData);
            
            if ($remember) {
                // cookie login
            }
            
            echo "Redirecting to dashboard...";
            redirect(base_url("dashboard"));
        }
        else {
            echo "Login fail. Redirecting to login page...";
            redirect(base_url("dashboard/login?status=-1"));
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url("dashboard/login"));
    }

    public function signup()
    {
        if ($this->m_student->checkSession()) {
            redirect(base_url("dashboard"));
        }

        $error = $this->input->get("status");
        if ($error) {
            $data['error_message'] = "Error happened when signing up. Try again later..";
        }

        $data['title'] = 'Student Login';
        $data['page'] = "studentlogin";
        $data['site'] = $this->siteconf;
        $data['menu'] = $this->menu;
        $data['isStudent'] = true;

        $this->load->view('template/head', $data);
        if ($error) {
            $this->load->view('template/error-popup');
        }
        $this->load->view('dashboard/signup');
        $this->load->view('template/foot');
    }

    public function signingUp()
    {
        $nama           = $this->input->post("nama");
        $asalSekolah    = $this->input->post("asal_sekolah");
        $kelas          = $this->input->post("kelas");
        $jenisKelamin   = $this->input->post("jenis_kelamin");
        $tanggalLahir   = $this->input->post("tanggal_lahir");
        $alamat         = $this->input->post("alamat");
        $noHp           = $this->input->post("no_hp");
        $email          = $this->input->post("email");
        $password       = sha1($this->input->post("password"));

        // begin test
            // echo '<pre>';
            // echo $nama.'<br/>';
            // echo $asalSekolah.'<br/>';
            // echo $kelas.'<br/>';
            // echo $jenisKelamin.'<br/>';
            // echo $tanggalLahir.'<br/>';
            // echo $alamat.'<br/>';
            // echo $noHp.'<br/>';
            // echo $email.'<br/>';
            // echo $password.'<br/>';
            // echo '</pre>';
        // end test

        $insertData = array(
            'nama'          => $nama,
            'jenis_kelamin' => $jenisKelamin,
            'alamat'        => $alamat,
            'tanggal_lahir' => $tanggalLahir,
            'asal_sekolah'  => $asalSekolah,
            'kelas'         => $kelas,
            'password'      => $password,
            'email'         => $email,
            'no_hp'         => $noHp
        );

        $affRows = $this->m_student->addStudent($insertData);

        if ($affRows <= 0) {
            redirect(base_url("dashboard/signup?status=-1"));
        }

        $where = array(
            'email' => $email,
            'password' => $password,
            'aktif' => 1
        );

        $this->processLogin($where);
    }

    public function updateProfile()
    {
        $nama           = $this->input->post("nama");
        $asalSekolah    = $this->input->post("asal_sekolah");
        $kelas          = $this->input->post("kelas");
        $jenisKelamin   = $this->input->post("jenis_kelamin");
        $tanggalLahir   = $this->input->post("tanggal_lahir");
        $alamat         = $this->input->post("alamat");
        $noHp           = $this->input->post("no_hp");
        $email          = $this->input->post("email");
        $password       = $this->input->post("password");

        // begin test
            // echo '<pre>';
            // echo $nama.'<br/>';
            // echo $asalSekolah.'<br/>';
            // echo $kelas.'<br/>';
            // echo $jenisKelamin.'<br/>';
            // echo $tanggalLahir.'<br/>';
            // echo $alamat.'<br/>';
            // echo $noHp.'<br/>';
            // echo $email.'<br/>';
            // echo $password.'<br/>';
            // echo '</pre>';
        // end test

        $updateData = array(
            'nama'          => $nama,
            'jenis_kelamin' => $jenisKelamin,
            'alamat'        => $alamat,
            'tanggal_lahir' => $tanggalLahir,
            'asal_sekolah'  => $asalSekolah,
            'kelas'         => $kelas,
            'email'         => $email,
            'no_hp'         => $noHp
        );

        if ($password !== null) {
            $updateData['password'] = sha1($password);
        }

        $result = array(
            'success' => true,
            'message'   => "Success updatting profile"
        );

        $affRows = $this->m_student->updateStudent($updateData, $this->userData->id_siswa);
        if ($affRows < 0) {
            $result = array(
                'success' => false,
                'message'   => "DB update profile error"
            );
        }
        else {
            $this->session->set_userdata("name", $nama);
        }

        echo json_encode($result);
    }
}

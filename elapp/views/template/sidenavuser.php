<nav class="sidenav">
    <div class="user-profile"></div>

    <div class="nav-content top">
        <a href="javascript:void(0)" class="nav-link">
            <i class="fas fa-tachometer-alt"></i> Dashboard
        </a>
        <a href="javascript:void(0)" class="nav-link">
            <i class="fas fa-tachometer-alt"></i> Course
        </a>
        <a href="javascript:void(0)" class="nav-link">
            <i class="fas fa-tachometer-alt"></i> Exam
        </a>
        <a href="javascript:void(0)" class="nav-link">
            <i class="fas fa-tachometer-alt"></i> Rank
        </a>
        <a href="javascript:void(0)" class="nav-link">
            <i class="fas fa-tachometer-alt"></i> Settings
        </a>
    </div>

    <div class="nav-content bottom">
        <a href="javascript:void(0)" class="nav-link">
            <i class="fas fa-tachometer-alt"></i> Logout
        </a>
    </div>
</nav>
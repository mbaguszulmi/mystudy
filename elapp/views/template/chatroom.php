<h2 class="title">Chat Room</h2>

<div class="chat-container">
    <div class="from"><i class="fas fa-user-alt fa-fw"></i> <?= $title ?></div>
    <div class="chat-messages" id="chat-messages">
        <?php
        if ($isAdmin) :
            foreach ($chatData as $key => $value) :
                if ($value->sender == 1) {
                    $senderName = '<i class="fas fa-crown fa-fw"></i> Administrator';
                    $addClass = " my";
                }
                else {
                    $senderName = $value->nama;
                    $addClass = " other";
                }

        ?>
                <div class="bubble-container">
                    <div class="bubble <?= $addClass ?>">
                        <div class="name"><?= $senderName ?></div>
                        <div class="message"><?= $value->pesan ?></div>
                        <div class="time-stamp"><?= substr($value->tanggal, 0, 16) ?></div>
                    </div>
                </div>
        <?php
            endforeach;
        
        else :
            foreach ($chatData as $key => $value) :
                if ($value->id_sender == $idSender && $value->sender == 0) {
                    $senderName = $value->nama;
                    $addClass = " my";
                }
                else {
                    if ($value->sender == 1) {
                        $senderName = '<i class="fas fa-crown fa-fw"></i> Administrator';
                    }
                    else {
                        $senderName = $value->nama;
                    }
                    $addClass = " other";
                }
        ?>
                <div class="bubble-container">
                    <div class="bubble <?= $addClass ?>">
                        <div class="name"><?= $senderName ?></div>
                        <div class="message"><?= $value->pesan ?></div>
                        <div class="time-stamp"><?= substr($value->tanggal, 0, 16) ?></div>
                    </div>
                </div>
        <?php
            endforeach;
        endif;
        ?>
    </div>
    <div class="text">
        <input type='text' id='message' name='message' autofocus placeholder="Type a message...">
    </div>
</div>

<script>
    $(document).ready(function() {
        var Server;
        const webSocket = 'ws://127.0.0.1:9300';
        var idUsr = <?= $idSender ?>;

        function log(data, me = false) {
            if (me) {
                console.log('You', data);
                $("#chat-messages").append(`
                    <div class="bubble-container">
                        <div class="bubble my">
                            <div class="name">${data.name}</div>
                            <div class="message">${data.message}</div>
                            <div class="time-stamp">${data.date}</div>
                        </div>
                    </div>
                `);
            } else {
                data = JSON.parse(data);
                console.log(data);
                $("#chat-messages").append(`
                    <div class="bubble-container">
                        <div class="bubble other">
                            <div class="name">${data.name}</div>
                            <div class="message">${data.message}</div>
                            <div class="time-stamp">${data.date}</div>
                        </div>
                    </div>
                `);
            }

            $("#chat-messages").animate({scrollTop: $("#chat-messages")[0].scrollHeight - $("#chat-messages")[0].clientHeight}, 500);
        }

        function send( data ) {
            Server.send( 'message', data );
        }

        console.log('Connecting...');
        Server = new FancyWebSocket(webSocket);
        
        let urlAJAX = `<?php
            if ($isAdmin) {
                echo base_url("admin/submitmessage");
            }
            else {
                echo base_url("dashboard/submitmessage");
            }
        ?>`;

        $('#message').keypress(function(e) {
            // console.log(urlAJAX);
            if ( e.keyCode == 13 && this.value ) {
                let date = new Date();
                let y = date.getFullYear();
                let m = date.getMonth()+1;
                let d = date.getDate();
                let h = date.getHours();
                let min = date.getMinutes();
                let mStr = m.toString().padStart(2, "0");
                let dStr = d.toString().padStart(2, "0");
                let hStr = h.toString().padStart(2, "0");
                let minStr = min.toString().padStart(2, "0");

                let msgData = {
                    id: idUsr,
                    message: this.value,
                    name: '<?= $name ?>',
                    date: `${y}-${mStr}-${dStr} ${hStr}:${minStr}`
                };

                $.ajax({
                    type: 'post',
                    url: urlAJAX,
                    data: msgData,
                    dataType: 'json',
                    success: (data, textStatus, jqHXR) => {
                        console.log(data);
                        log( msgData, true );
                        send(JSON.stringify(msgData));
                    },
                    error: (data, textStatus, jqHXR) => {
                        console.log(data);
                    } 
                });

                $(this).val('');
            }
        });

        //Let the user know we're connected
        Server.bind('open', function() {
            console.log( "Connected." );
        });

        //OH NOES! Disconnection occurred.
        Server.bind('close', function( data ) {
            console.log( "Disconnected." );
        });

        //Log any messages sent from server
        Server.bind('message', function( payload ) {
            log( payload );
        });

        Server.connect();

        // on sidenav click
        $(".sidenav .nav-link").click((event) => {
            Server.disconnect();
        });

        setTimeout(() => {
            $("#chat-messages").animate({scrollTop: $("#chat-messages")[0].scrollHeight - $("#chat-messages")[0].clientHeight}, 500);
            console.log("nice");
        }, 1500);
    });
</script>
<div class="login-wrapper">
    <div class="form-container">
        <div class="title-section">
            <a href="<?= base_url() ?>"><img src="/assets/images/logo.png" alt="<?= $site['name'] ?>"></a>
            <h1 class="title"><?= $title ?></h1>
        </div>

        <form action="<?= $processUrl ?>" class="login-form" method="post">
            <?php
            if ($isStudent) :
            ?>
            <div class="form-group username">
                <input autofocus type="email" name="email" placeholder="Email" class="input" required>
            </div>
            <?php
            else :
            ?>
            <div class="form-group username">
                <input autofocus type="text" name="username" placeholder="Username" class="input" required>
            </div>
            <?php
            endif;
            ?>
            <div class="form-group password">
                <input type="password" name="password" placeholder="Password" class="input" required>
            </div>
            <div class="form-group">
                <input type="checkbox" name="remember" class="checkbox" id="remember">
                <label for="remember">Remember me</label>
            </div>
            <div class="form-group submit-area">
                <button type="submit" class="btn green round submit">Sign In</button>
            </div>
        </form>
    </div>
</div>
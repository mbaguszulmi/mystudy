<header class="header">
    <div class="bg">
        <svg width="235.658mm" height="191.967mm" viewBox="0 0 235.658 191.967" version="1.1" id="illustrator-svg">
            <defs id="defs2">
                <linearGradient id="linearGradient856">
                    <stop style="stop-color:#2ab49d;stop-opacity:1;" offset="0" id="stop852" />
                    <stop style="stop-color:#97d6cb;stop-opacity:1" offset="1" id="stop854" />
                </linearGradient>
                <linearGradient xlink:href="#linearGradient856" id="linearGradient858" x1="21.520491" y1="128.03308"
                    x2="230.89633" y2="279.43295" gradientUnits="userSpaceOnUse"
                    gradientTransform="matrix(0.94472134,0,0,0.94472134,67.079525,61.838982)" />
            </defs>
            <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-105.033)">
                <path
                    style="fill:url(#linearGradient858);fill-opacity:1;stroke:none;stroke-width:0.24995749px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                    d="m 235.658,105.033 h -82.11257 c 0,0 -46.63344,5.93458 -62.132255,58.13297 -5.22444,17.59527 -3.44827,34.53382 -14.99747,52.134 C 67.45113,228.96138 38.03608,244.26072 22.85339,260.29231 1.2907508,283.06054 1.3076074e-4,297.00036 1.3076074e-4,297.00036 H 235.658 Z"
                    id="path828" inkscape:connector-curvature="0" sodipodi:nodetypes="ccsssccc" />
            </g>
        </svg>
    </div>

    <div class="header-wrapper">
        <div class="illustration-wrapper">
            <div class="illustration"></div>
        </div>

        <div class="content">
            <div class="inner">
                <h2>Bring back glory!</h2>
                <p>Come join us! Make your future better with our learning academy.</p>
    
                <form autocomplete="off" action="" class="sign-up-form">
                    <input type="email" name="email" id="email" class="email-input" placeholder="someone@domain.com" required>
                    <input type="submit" value="Learn Now!" class="email-submit">
                </form>
            </div>
        </div>
    </div>
</header>
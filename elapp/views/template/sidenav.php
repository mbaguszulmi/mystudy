<nav class="sidenav" id="control-nav" navtype="<?= $menu['navtype'] ?>">
    <div class="user-profile">
        <div class="avatar"></div>
        <div class="name"><?= $sessionData['name'] ?></div>
    </div>

    <div class="nav-content top">
        <?php
        foreach ($menu['top'] as $key => $value):
        ?>
            <a href="javascript:void(0)" class="nav-link
            <?php
            if ($key == 0) {
                echo "active";
            }
            ?>
            " target-page="<?= $value['target-page'] ?>" direct="<?= $value['direct'] ?>">
                <i class="fas fa-<?= $value['icon'] ?> fa-fw"></i> <?= $value['name'] ?>
            </a>
        <?php
        endforeach;
        ?>
    </div>

    <div class="nav-content bottom">
        <?php
        foreach ($menu['bottom'] as $key => $value):
        ?>
            <a href="javascript:void(0)" class="nav-link" target-page="<?= $value['target-page'] ?>" direct="<?= $value['direct'] ?>">
                <i class="fas fa-<?= $value['icon'] ?> fa-fw"></i> <?= $value['name'] ?>
            </a>
        <?php
        endforeach;
        ?>
    </div>
</nav>
<footer class="footer">
    <div class="main-wrapper footer-wrapper">

        <?php
        if ($page != 'dashboard' && $page != 'admin'):
        ?>
        <div class="user-count">
            1,256 Students joined <br>
            Let's start your journey now!
        </div>
        <?php
        endif;
        ?>

        <div class="footer-content simple-about">
            <img src="/assets/images/logo.png" alt="<?= $site['name'] ?>">

            <p>
            <i>"<?= $site['simple-about'] ?>"</i> - <?= $site['name'] ?> 2K19
        </div>
        <div class="footer-content u-link-wrapper">
            <h3 class="title">Usefull Links</h3>
            <div class="u-link">
                <a href="/page/about"><i class="fas fa-info-circle fa-fw"></i> About</a>
            </div>
            <div class="u-link">
                <a href="/page/privacy"><i class="fas fa-lock fa-fw"></i> Privacy Policy</a>
            </div>
            <div class="u-link">
                <a href="/page/terms"><i class="fas fa-exclamation-circle fa-fw"></i> Terms of Use</a>
            </div>
            <div class="u-link">
                <a href="/page/faq"><i class="fas fa-question-circle fa-fw"></i> FAQ</a>
            </div>
            <div class="u-link">
                <a href="/page/contact"><i class="fas fa-phone fa-fw"></i> Contact Us</a>
            </div>
        </div>

        <div class="footer-content socials">
            <h3 class="title">Stay Connected</h3>

            <div class="social-icons">
                <a href="<?= $site['fb'] ?>"><i class="fab fa-facebook-square fa-fw"></i></a>
                <a href="<?= $site['twitter'] ?>"><i class="fab fa-twitter fa-fw"></i></a>
                <a href="<?= $site['ig'] ?>"><i class="fab fa-instagram fa-fw"></i></a>
                <a href="<?= $site['youtube'] ?>"><i class="fab fa-youtube fa-fw"></i></a>
            </div>
        </div>

        <div class="copy">
            &copy; 2019 <?= $site['name'] ?> <br>
            All rights reserved
        </div>
    </div>
</footer>
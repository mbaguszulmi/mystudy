<div class="dashboard-content">
    <?php require 'sidenav.php' ?>
    <div class="fragment" id="fragment">
        <div class="loader-wrapper" id="loader">
            <div class="animation-container">
                <div id="animation-loading" class="animation-loading show-animation">
                    <svg class="animation-svg"dth="104.321mm" height="95.211998mm"
                        viewBox="0 0 104.321 95.211998" version="1.1" id="svg8">
                        <defs id="defs2" />
                        <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(0,-201.788)">
                            <path
                                style="opacity:1;fill:#f9ec40;fill-opacity:1;stroke:none;stroke-width:13.93520451;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                d="M 52.16064,201.788 A 52.160709,52.160709 0 0 0 0,253.94864 v 39.30975 H 104.32128 V 253.94864 A 52.160709,52.160709 0 0 0 52.16064,201.788 Z"
                                id="path820" inkscape:connector-curvature="0" />
                            <g id="g847" class="smile">
                                <ellipse ry="10.423511" rx="10.423512" cy="237.09969" cx="33.535606" id="path875"
                                    style="opacity:1;fill:#f2f2f2;fill-opacity:1;stroke:none;stroke-width:2.7862947;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
                                <circle r="6.7485552" cy="236.89923" cx="30.52883" id="path877"
                                    style="opacity:1;fill:#666666;fill-opacity:1;stroke:none;stroke-width:3.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
                            </g>
                            <g id="g839" class="sad">
                                <ellipse
                                    style="opacity:1;fill:#f2f2f2;fill-opacity:1;stroke:none;stroke-width:2.7862947;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    id="ellipse859" cx="33.535603" cy="237.09967" rx="10.423512" ry="10.423511" />
                                <circle
                                    style="opacity:1;fill:#666666;fill-opacity:1;stroke:none;stroke-width:3.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    id="circle861" cx="33.703831" cy="240.07428" r="6.7485552" />
                            </g>
                            <g id="g851" class="smile">
                                <ellipse
                                    style="opacity:1;fill:#f2f2f2;fill-opacity:1;stroke:none;stroke-width:2.7862947;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    id="ellipse879" cx="70.710571" cy="237.09969" rx="10.423512" ry="10.423511" />
                                <circle
                                    style="opacity:1;fill:#666666;fill-opacity:1;stroke:none;stroke-width:3.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    id="circle881" cx="67.703796" cy="236.89923" r="6.7485552" />
                            </g>
                            <g id="g843" class="sad">
                                <ellipse ry="10.423511" rx="10.423512" cy="237.09967" cx="70.710579" id="ellipse863"
                                    style="opacity:1;fill:#f2f2f2;fill-opacity:1;stroke:none;stroke-width:2.7862947;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
                                <circle r="6.7485552" cy="240.07428" cx="70.878807" id="circle865"
                                    style="opacity:1;fill:#666666;fill-opacity:1;stroke:none;stroke-width:3.5;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
                            </g>
                            <path
                                style="fill:none;stroke:#666666;stroke-width:2.25580192;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:7.69999981;stroke-dasharray:none;stroke-opacity:1"
                                d="m 72.468379,263.85606 c 0,0 -5.403955,12.44372 -20.307159,12.54808 -13.732153,0.0962 -20.307165,-12.54808 -20.307165,-12.54808"
                                class="smile" id="path837" inkscape:connector-curvature="0" sodipodi:nodetypes="csc" />
                            <path sodipodi:nodetypes="csc" inkscape:connector-curvature="0" id="path867"
                                d="m 72.468379,276.38994 c 0,0 -5.40395,-12.44372 -20.30716,-12.54808 -13.73215,-0.0962 -20.30716,12.54808 -20.30716,12.54808"
                                class="sad" style="fill:none;stroke:#666666;stroke-width:2.25580192;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:7.69999981;stroke-dasharray:none;stroke-opacity:1" />
                            <path
                                style="fill:none;stroke:#666666;stroke-width:1.61000001;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:7.69999981;stroke-dasharray:none;stroke-opacity:1"
                                d="m 30.56989,219.83091 c 0,0 0.201698,4.26399 -3.992036,6.36253 -3.864182,1.93369 -7.476243,-0.72387 -7.476243,-0.72387"
                                class="sad" id="path869" inkscape:connector-curvature="0" sodipodi:nodetypes="csc" />
                            <path sodipodi:nodetypes="csc" inkscape:connector-curvature="0" id="path871"
                                d="m 73.751392,219.83091 c 0,0 -0.201698,4.26399 3.992036,6.36253 3.864182,1.93369 7.476243,-0.72387 7.476243,-0.72387"
                                class="sad" style="fill:none;stroke:#666666;stroke-width:1.61000001;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:7.69999981;stroke-dasharray:none;stroke-opacity:1" />
                            <g id="g977" transform="translate(0.99360675,-121.81805)">
                                <animateTransform id="shift-animation" attributeName="transform"
                                    attributeType="XML"
                                    type="translate"
                                    from="0.99360675 -121.81805"
                                    to="35.766676,-121.81805"
                                    dur="0.5s"
                                    repeatCount="indefinite"/>
                                <path inkscape:connector-curvature="0" id="path932"
                                    d="m 16.392928,411.33455 v 3.74189 a 12.487858,13.229166 0 0 0 8.702828,3.74189 12.487858,13.229166 0 0 0 8.683706,-3.74189 v -3.74189 z"
                                    style="opacity:1;fill:#f9ec40;fill-opacity:1;stroke:none;stroke-width:3.40052366;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
                                <path class="remover" inkscape:connector-curvature="0"
                                    style="opacity:1;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:3.40052366;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    d="m -0.99360675,418.81833 v -3.74189 a 12.487858,13.229166 0 0 1 8.70282695,-3.74189 12.487858,13.229166 0 0 1 8.6837078,3.74189 v 3.74189 z"
                                    id="path945" />
                                <path
                                    style="opacity:1;fill:#f9ec40;fill-opacity:1;stroke:none;stroke-width:3.40052366;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    d="m 51.165997,411.33455 v 3.74189 a 12.487858,13.229166 0 0 0 8.702828,3.74189 12.487858,13.229166 0 0 0 8.683706,-3.74189 v -3.74189 z"
                                    id="path957" inkscape:connector-curvature="0" />
                                <path class="remover" id="path959"
                                    d="m 33.779462,418.81833 v -3.74189 a 12.487858,13.229166 0 0 1 8.702827,-3.74189 12.487858,13.229166 0 0 1 8.683708,3.74189 v 3.74189 z"
                                    style="opacity:1;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:3.40052366;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    inkscape:connector-curvature="0" />
                                <path inkscape:connector-curvature="0" id="path961"
                                    d="m 85.939066,411.33455 v 3.74189 a 12.487858,13.229166 0 0 0 8.702828,3.74189 12.487858,13.229166 0 0 0 8.683706,-3.74189 v -3.74189 z"
                                    style="opacity:1;fill:#f9ec40;fill-opacity:1;stroke:none;stroke-width:3.40052366;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
                                <path class="remover" inkscape:connector-curvature="0"
                                    style="opacity:1;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:3.40052366;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    d="m 68.552531,418.81833 v -3.74189 a 12.487858,13.229166 0 0 1 8.702827,-3.74189 12.487858,13.229166 0 0 1 8.683708,3.74189 v 3.74189 z"
                                    id="path963" />
                                <path
                                    style="opacity:1;fill:#f9ec40;fill-opacity:1;stroke:none;stroke-width:3.40052366;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    d="m -18.380141,411.33455 v 3.74189 a 12.487858,13.229166 0 0 0 8.7028282,3.74189 12.487858,13.229166 0 0 0 8.68370605,-3.74189 v -3.74189 z"
                                    id="path965" inkscape:connector-curvature="0" />
                                <path class="remover" id="path967"
                                    d="m -35.766676,418.81833 v -3.74189 a 12.487858,13.229166 0 0 1 8.702827,-3.74189 12.487858,13.229166 0 0 1 8.683708,3.74189 v 3.74189 z"
                                    style="opacity:1;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:3.40052366;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
                                    inkscape:connector-curvature="0" />
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="loader-error-section" id="error-section">
                    <h3 class="error-title" id="error-title"></h3>
                    <p class="error-details" id="error-details"></p>
                    <div class="btn green" id="refresh-load-btn"><i class="fas fa-redo fa-fw"></i> Refresh</div>
                </div>
            </div>
        </div>
        <div class="fragment-content" id="fragment-content">
        </div>

        <?php require 'footer.php' ?>
    </div>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?php
        if ($title != $site['name']) {
            echo $title.' | '.$site['name'];
        }
        else {
            echo $title;
        }
        ?>
    </title>

    <link rel="shortcut icon" href="/assets/images/favicon.png" type="image/png">

    <link rel="stylesheet" href="/assets/css/style.min.css">
    <link rel="stylesheet" href="/assets/css/custombox.min.css">
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/all.min.css">

    <script src="/assets/js/jquery-3.3.1.min.js"></script>
    <script src="/assets/js/script.js"></script>
    <script defer src="/assets/fonts/fontawesome/js/all.min.js"></script>
    <?php
    if ($page == 'dashboard' || $page == 'admin' || $page != 'index'):
    ?>
        <link rel="stylesheet" href="/assets/css/style-add.min.css">
        <script src="/assets/js/sidenav-controller.js"></script>
        <link rel="stylesheet" type="text/css" href="/assets/DataTables/datatables.min.css"/>
        <script type="text/javascript" src="/assets/DataTables/datatables.min.js"></script>
        <script src="assets/js/fancywebsocket.js"></script>
        <link rel="stylesheet" href="/assets/css/custom-form.min.css">
    <?php
    endif;
    ?>
</head>
<body>

<nav class="topnav">
    <div class="nav-wrapper">
        <div class="brand">
            <a href="/">
                <img src="/assets/images/logo.png" alt="<?= $site['name'] ?>">
            </a>
        </div>
        <div class="nav-content left">
            <a class="nav-link <?php 
            if ($page == "index") {
                echo "active";
            }
            ?>" href="/">Home</a>
            <a class="nav-link <?php 
            if ($page == "contact") {
                echo "active";
            }
            ?>" href="/page/contact">Contacts</a>
            <a class="nav-link <?php 
            if ($page == "faq") {
                echo "active";
            }
            ?>" href="/page/faq">FAQ</a>
            <a class="nav-link <?php 
            if ($page == "about") {
                echo "active";
            }
            ?>" href="/page/about">About</a>
        </div>
        <div class="nav-content">
            <?php
            if (!isset($sessionData['name'])) :
            ?>
                <a href="<?= base_url("dashboard/login") ?>" class="btn lgrey">Sign In</a>
                <a href="<?= base_url("dashboard/signup") ?>" class="btn lgrey">Sign Up</a>
            <?php
            else :
                if (isset($sessionData['admin'])) {
                    $url = base_url("admin");
                }
                else {
                    $url = base_url("dashboard");
                }
            ?>
                <a href="<?= $url ?>" class="avatar"></a>
            <?php
            endif;
            ?>
        </div>
    </div>
</nav>
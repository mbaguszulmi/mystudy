<div class="main-wrapper">
    <h1><?= $title ?></h1>
    <p>
        For more information you can contact us with this information: <br>
        <i class="fas fa-phone fa-fw"></i>Phone:  62 89682056995 <br>
        <i class="fas fa-envelope fa-fw"></i>E-Mail: info@mystudy.edu <br>
        <i class="fas fa-fax fa-fw"></i>Fax: 03410673845
    </p>
</div>

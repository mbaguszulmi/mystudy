<div class="main-wrapper">
    <h1><?= $title ?></h1>
    <div class="about-container">
        <div class="title">About developers</div>
        <div class="person">
            <div class="person-avatar">
                <img src="<?= base_url("assets/images/developers/lila.jpg") ?>" width="200px" alt="developers">
            </div>
            <div class="detail">
                <div><i class="fas fa-user fa-fw"></i> Lila Andana Fitri</div>
                <div><i class="fas fa-id-card fa-fw"></i> 170535629509</div>
                <div><i class="fas fa-tools fa-fw"></i> Project leader</div>
            </div>
        </div>
        <div class="person">
            <div class="person-avatar">
                <img src="<?= base_url("assets/images/developers/bagus.jpg") ?>" width="200px" alt="developers">
            </div>
            <div class="detail">
                <div><i class="fas fa-user fa-fw"></i> Muhammad Bagus Zulmi</div>
                <div><i class="fas fa-id-card fa-fw"></i> 170535629515</div>
                <div><i class="fas fa-tools fa-fw"></i> System analysis, Implementation, and Testing</div>
            </div>
        </div>
        <div class="person">
            <div class="person-avatar">
                <img src="<?= base_url("assets/images/developers/ifa.jpg") ?>" width="200px" alt="developers">
            </div>
            <div class="detail">
                <div><i class="fas fa-user fa-fw"></i> Mazidah Zufa</div>
                <div><i class="fas fa-id-card fa-fw"></i> 170535629565</div>
                <div><i class="fas fa-tools fa-fw"></i> Impelemtation</div>
            </div>
        </div>
        <div class="person">
            <div class="person-avatar">
                <img src="<?= base_url("assets/images/developers/nendi.jpg") ?>" width="200px" alt="developers">
            </div>
            <div class="detail">
                <div><i class="fas fa-user fa-fw"></i> Nendi Oktaventi Adita Putri</div>
                <div><i class="fas fa-id-card fa-fw"></i> 170535629561</div>
                <div><i class="fas fa-tools fa-fw"></i> ?</div>
            </div>
        </div>
        <div class="person">
            <div class="person-avatar">
                <img src="<?= base_url("assets/images/developers/pratama.png") ?>" width="200px" alt="developers">
            </div>
            <div class="detail">
                <div><i class="fas fa-user fa-fw"></i> Mohammad Na'im Pratama</div>
                <div><i class="fas fa-id-card fa-fw"></i> 170535629510</div>
                <div><i class="fas fa-tools fa-fw"></i> ?</div>
            </div>
        </div>
    </div>
</div>

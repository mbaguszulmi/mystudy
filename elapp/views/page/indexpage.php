<div class="info">
    <div class="bg1"></div>
    <div class="bg2"></div>
    <div class="main-wrapper">
        <h2 class="title"><?= $site['name'] ?> is...</h2>

        <div class="content">
            <div class="desc left">
                <div class="desc-content">
                    <div>
                        <h3 class="title">Fast and simple</h3>
                        <p>
                            We design a futuristic web that load fast in any device. Make a better experience for you to enjoy your learning activity.
                    </div>
                    <div class="icon">
                        <div class="icon-container">
                            <img src="/assets/images/fast.png" alt="fast">
                        </div>
                    </div>
                </div>
            </div>

            <div class="desc separator"></div>

            <div class="desc right">
                <div class="desc-content">
                    <div class="icon">
                        <div class="icon-container">
                        <img src="/assets/images/easy.png" alt="easy">
                        </div>
                    </div>
                    <div>
                        <h3 class="title">Easy to understand</h3>
                        <p>
                            Any module in this site was made by collecting any book and we take the best explanation from that book. Also we have a good team for making this modules better.
                    </div>
                </div>
            </div>

            <div class="desc separator"></div>

            <div class="desc left">
                <div class="desc-content">
                    <div>
                        <h3 class="title">Download any exam and module</h3>
                        <p>
                            Every exam and modules in this site can be downloaded acording to your grade. An exam can be downloaded if you have passed that exam.
                    </div>
                    <div class="icon">
                        <div class="icon-container">
                        <img src="/assets/images/download.png" alt="download">
                        </div>
                    </div>
                </div>
            </div>

            <div class="desc separator"></div>

            <div class="desc right">
                <div class="desc-content">
                    <div class="icon">
                        <div class="icon-container">
                        <img src="/assets/images/free.png" alt="free">
                        </div>
                    </div>
                    <div>
                        <h3 class="title">100% Free!</h3>
                        <p>
                            We don't charge you for using this system. It's made for your better future and helping this country.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="testimonial">
    <div class="bg1"></div>
    
    <div class="main-wrapper slider-wrapper">
        <h1 class="title">Let's see what they said</h1>

        <div class="slider">
            <div class="button prev" id="prev">❮</div>
            <div class="button next" id="next">❯</div>

            <div class="slider-content" id="slider-content">
                <div class="content">
                    <div class="person">
                        <div class="avatar" data-image="/assets/images/testimonial/tom-cruise.jpg"></div>
                        <b>Tom Cruise</b>
                    </div>

                    <div class="message">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    </div>
                </div>

                <div class="content">
                    <div class="person">
                        <div class="avatar" data-image="/assets/images/testimonial/tom-holland.jpg"></div>
                        <b>Tom Holland</b>
                    </div>

                    <div class="message">
                        Laboriosam officiis placeat exercitationem blanditiis, sint suscipit, est ad voluptas rerum quod quae quam ullam, natus totam odio pariatur et unde voluptate?
                    </div>
                </div>

                <div class="content">
                    <div class="person">
                        <div class="avatar" data-image="/assets/images/testimonial/robert-downey.jpg"></div>
                        <b>Robert Downey</b>
                    </div>

                    <div class="message">
                        Quibusdam, rerum! Sed ratione illo quae reprehenderit, aliquam placeat quidem nulla itaque incidunt aliquid in dignissimos labore ea dolorum veritatis facilis! Architecto.
                    </div>
                </div>

                <div class="content">
                    <div class="person">
                        <div class="avatar" data-image="/assets/images/testimonial/scarlett-johansson.jpg"></div>
                        <b>Scarlett Johansson</b>
                    </div>

                    <div class="message">
                        Cumque beatae illo id blanditiis nostrum adipisci similique quasi magni recusandae.
                    </div>
                </div>

                <div class="content">
                    <div class="person">
                        <div class="avatar" data-image="/assets/images/testimonial/brie-larson.jpg"></div>
                        <b>Brie Larson</b>
                    </div>

                    <div class="message">
                        Maiores ullam incidunt rem nemo quas ducimus voluptatem quae eum.
                    </div>
                </div>

                <div class="content">
                    <div class="person">
                        <div class="avatar" data-image="/assets/images/testimonial/chris-evans.jpg"></div>
                        <b>Chris Evans</b>
                    </div>

                    <div class="message">
                        Fugit eveniet eaque cum vero tenetur placeat unde nostrum illum perspiciatis.
                    </div>
                </div>

                <div class="content">
                    <div class="person">
                        <div class="avatar" data-image="/assets/images/testimonial/mark-ruffalo.jpg"></div>
                        <b>Mark Ruffalo</b>
                    </div>

                    <div class="message">
                        Voluptatibus eaque blanditiis labore illo iure?
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
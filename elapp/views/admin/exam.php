<h2 class="title">Exam</h2>

<nav class="inner-nav" id="control-nav" navtype="adminnav">
    <div class="btn round green nav-link newexam-link" target-page="newexam" direct=""><i class="fas fa-plus-circle fa-fw"></i> New Exam</div>
</nav>

<table id="exam-table" class="display">
    <thead>
        <tr>
            <th>Exam Title</th>
            <th>Time Limit</th>
            <th>Grade</th>
            <th>Subject</th>
            <th>Upload Date</th>
            <th>Update Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($examList as $key => $value) :
        ?>
        <tr>
            <td><?= $value->nama_soal ?></td>
            <td><?= $value->waktu_menit ?></td>
            <td><?= $value->kelas ?></td>
            <td><?= $value->nama_matpel ?></td>
            <td><?= $value->tanggal_upload ?></td>
            <td><?= $value->tanggal_update ?></td>
            <td>
            <div class="btn-group">
                    <div class="action-btn yellow newexam-link" target-page="editexam/<?= $value->id_soal ?>" direct=""><i class="fas fa-edit fa-fw"></i></div>
                    <div class="action-btn grey"><i class="fas fa-trash-alt fa-fw"></i></div>
                    <div class="action-btn red newexam-link" target-page="<?= base_url("pdfviewer?file=").base_url("admin/getpage/exampdf/$value->id_soal") ?>" direct="1" blank="1"><i class="fas fa-file-pdf fa-fw"></i></div>
                </div>
            </td>
        </tr>
        <?php
        endforeach;
        ?>
    </tbody>
</table>

<script>
$(".newexam-link").click((event) => {
    clickEvent(event, control, false);
});

$("#exam-table").DataTable();
</script>
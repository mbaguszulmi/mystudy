<h2 class="title">New Course</h2>

<form action="/admin/addcourse" id="newcourse-form" class="custom-form" enctype="multipart/form-data" method="post">
    <div class="form-group">
        <input class="input-data custom-input" type="text" name="nama_materi" id="nama_materi" placeholder="Course Title" required>
    </div>
    <div class="form-group">
        <select class="input-data custom-input" name="kelas" id="kelas" required>
            <option value="1" disabled selected>Choose Grade</option>
            <option value="1">1 SD</option>
            <option value="2">2 SD</option>
            <option value="3">3 SD</option>
            <option value="4">4 SD</option>
            <option value="5">5 SD</option>
            <option value="6">6 SD</option>
            <option value="7">1 SMP</option>
            <option value="8">2 SMP</option>
            <option value="9">3 SMP</option>
        </select>
    </div>
    <div class="form-group">
        <select class="input-data custom-input" name="id_matpel" id="id_matpel" required>
            <option value="1" disabled selected>Choose Subject</option>
            <!-- <option value="1">Matematika</option>
            <option value="2">Bhs. Indonesia</option> -->
            <?php
            foreach ($matpelList as $key => $row):
            ?>
                <option value="<?php echo $row->id_matpel ?>"><?php echo $row->nama_matpel ?></option>
            <?php
            endforeach;
            ?>
        </select> <br/>
        <input type="checkbox" name="" class="checkbox" id="new-matpel">
        <label for="new-matpel">New Subject</label> <br/>
        <input class="input-data custom-input" type="text" name="nama_matpel" id="nama_matpel" placeholder="Subject" required disabled>
    </div>
    <div class="form-group">
        <input class="input-data custom-input" type="file" name="file_materi" id="file_materi" required>
    </div>
    <div class="form-group">
        <button type="submit" id="submit-btn" class="btn green round">Save Course</button>
    </div>
</form>

<div class="loader-make" id="loader-make">
    <div class="spin-container">
        <div class="spin"></div>
    </div>
    <div class="message">Uploading Content...</div>
</div>

<script>
$("#new-matpel").change((event) => {
    if ($(event.currentTarget).prop('checked')) {
        $("#id_matpel").prop('disabled', true);
        $("#nama_matpel").prop('disabled', false);
    }
    else {
        $("#nama_matpel").prop('disabled', true);
        $("#id_matpel").prop('disabled', false);
    }
});



$("#submit-btn").click((event) => {
    let inputFill = () => {
        let inputData = $(".input-data:not(:disabled)");

        for (let i = 0; i < inputData.length; i++) {
            let isFilled = ($(inputData[i]).val() === "" || $(inputData[i]).val() === null)? false : true;
            if (!isFilled) return false;
        }
        return true;
    }

    if (inputFill()) {
        event.preventDefault();
        $(event.currentTarget).prop("disabled", true);
        $("#loader-make").css("display", "grid");

        let targetUrl = $("#newcourse-form").attr('action');
        console.log(targetUrl);
        let formData = new FormData($("#newcourse-form")[0]);

        $.ajax({
            type: 'post',
            url: targetUrl,
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: (data, textStatus, jqHXR) => {
                console.log(data);
                beginTransaction('/'+control+'/getpage/course');
            },
            error: (data, textStatus, jqHXR) => {
                console.log(data);
                $("#loader-make").css("display", "none");
                $(event.currentTarget).prop("disabled", false);
            } 
        });
    }
    else {
        $(event.currentTarget).prop("disabled", false);
    }
});
</script>
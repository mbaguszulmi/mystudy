<h2 class="title">New Exam</h2>

<form action="/admin/addexam" method="post" class="custom-form-exam custom-form" id="newexam-form">
    <div class="form-group">
        <input class="input-exam custom-input" type="text" name="nama_soal" id="nama_soal" placeholder="Exam Title" required>
    </div>
    <div class="form-group">
        <select class="input-exam custom-input" name="kelas" id="kelas" required>
            <option value="1" disabled selected>Choose Grade</option>
            <option value="1">1 SD</option>
            <option value="2">2 SD</option>
            <option value="3">3 SD</option>
            <option value="4">4 SD</option>
            <option value="5">5 SD</option>
            <option value="6">6 SD</option>
            <option value="7">1 SMP</option>
            <option value="8">2 SMP</option>
            <option value="9">3 SMP</option>
        </select>
    </div>
    <div class="form-group">
        <select class="input-exam custom-input" name="id_matpel" id="id_matpel" required>
            <option value="1" disabled selected>Choose Subject</option>
            <!-- <option value="1">Matematika</option>
            <option value="2">Bhs. Indonesia</option> -->
            <?php
            foreach ($matpelList as $key => $row):
            ?>
                <option value="<?php echo $row->id_matpel ?>"><?php echo $row->nama_matpel ?></option>
            <?php
            endforeach;
            ?>
        </select> <br/>
        <input type="checkbox" class="checkbox" name="" id="new-matpel">
        <label for="new-matpel">New Subject</label>
        <input class="input-exam custom-input" type="text" name="nama_matpel" id="nama_matpel" placeholder="Subject" required disabled>
    </div>
    <div class="form-froup">
        <label for="waktu_menit">Time limit (minute)</label>
        <input class="input-exam custom-input" value="1" type="number" min="1" name="waktu_menit" id="waktu_menit" required>
    </div>
</form>

<form action="" class="custom-form" id="setupcourse-form">
    <label for="num-question">Number of questions</label>
    <input class="input-setup custom-input" type="number" name="" id="num-question" min="1" max="50" required value="1">
    <label for="num-answers">Number of answers</label>
    <input class="input-setup custom-input" type="number" name="" id="num-answer" min="2" max="5" required value="2">
    <button id="create-btn" class="btn green round">Create</button>
</form>

<div class="loader-make" id="loader-make">
    <div class="spin-container">
        <div class="spin"></div>
    </div>
    <div class="message">Uploading Content...</div>
</div>

<script>
$("#new-matpel").change((event) => {
    if ($(event.currentTarget).prop('checked')) {
        $("#id_matpel").prop('disabled', true);
        $("#nama_matpel").prop('disabled', false);
    }
    else {
        $("#nama_matpel").prop('disabled', true);
        $("#id_matpel").prop('disabled', false);
    }
});

$("#setupcourse-form").submit((event) => {
    event.preventDefault();

    let question = parseInt($("#num-question").val());
    let ans = parseInt($("#num-answer").val());
    let ansCount = 0;

    for (let i = 0; i < question; i++) {
        let questSection = `
        <div class="quest-section">
            <div class="title">Question Number ${i+1}</div>
            <div class="form-group">
                <textarea class="input-exam custom-input" name="pertanyaan[${i}]" id="pertanyaan[${i}]" cols="30" rows="10" required></textarea>
            </div>
            <div class="ans-section">
            </div>
        </div>
        `;

        $("#newexam-form").append(questSection);

        for (let j = 0; j < ans; j++) {
            let ansContent = `
            <div class="form-group">
                <input type="radio" class="radiobox input-exam" name="benar[${i}]" id="benar[${ansCount}]" value="${ansCount}" required>
                <label for="benar[${ansCount}]"></label>
            </div>
            <textarea  class="input-exam custom-input" name="jawaban[${ansCount}]" id="jawaban[${ansCount++}]" cols="30" rows="10" required></textarea>
            `;
            // console.log(ansContent);
            $(".ans-section").last().append(ansContent);
        }
    }

    $("#newexam-form").append(`<button type="submit" id="save-btn" class="btn green round">Save</button>`);
    $(event.currentTarget).empty();
});

$("#newexam-form").submit((event) => {
    let inputFill = () => {
        let inputData = $(".input-exam:not(:disabled)");

        for (let i = 0; i < inputData.length; i++) {
            let isFilled = ($(inputData[i]).val() === "" || $(inputData[i]).val() === null)? false : true;
            if (!isFilled) return false;
        }
        return true;
    }

    if (inputFill()) {
        event.preventDefault();
        $("#fragment").scrollTop(0);

        $("#save-btn").prop("disabled", true);

        $("#loader-make").css("display", "grid");

        let targetUrl = $("#newexam-form").attr('action');
        // console.log(targetUrl);
        let formData = new FormData($("#newexam-form")[0]);

        $.ajax({
            type: 'post',
            url: targetUrl,
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: (data, textStatus, jqHXR) => {
                // console.log(data);
                beginTransaction('/'+control+'/getpage/exam');
            },
            error: (data, textStatus, jqHXR) => {
                // console.log(data);
                $("#loader-make").css("display", "none");
                $("#save-btn").prop("disabled", false);
            } 
        });

        
    }
});
</script>
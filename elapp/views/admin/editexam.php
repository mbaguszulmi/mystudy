<?php
$grades = array(
    1 => '1 SD',
    2 => '2 SD',
    3 => '3 SD',
    4 => '4 SD',
    5 => '5 SD',
    6 => '6 SD',
    7 => '1 SMP',
    8 => '2 SMP',
    9 => '3 SMP',
);
?>

<h2 class="title">Edit Exam</h2>

<form action="/admin/updateexam" method="post" class="custom-form-exam custom-form" id="editexam-form" target="_blank">
    <input type="hidden" name="id_soal" value="<?= $examData->id_soal ?>">
    <div class="form-group">
        <input class="input-exam custom-input" type="text" name="nama_soal" id="nama_soal" placeholder="Exam Title" value="<?= $examData->nama_soal ?>" required>
    </div>
    <div class="form-group">
        <select class="input-exam custom-input" name="kelas" id="kelas" required>
            <option value="1" disabled>Choose Grade</option>
            <?php
            foreach ($grades as $key => $value):
            ?>
                <option value="<?= $key ?>" <?php
                    if ($key == (int)$examData->kelas) {
                        echo "selected";
                    }
                ?>><?= $value ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </div>
    <div class="form-group">
        <select class="input-exam custom-input" name="id_matpel" id="id_matpel" required>
            <option value="1" disabled>Choose Subject</option>
            <!-- <option value="1">Matematika</option>
            <option value="2">Bhs. Indonesia</option> -->
            <?php
            foreach ($matpelList as $key => $row):
            ?>
                <option value="<?php echo $row->id_matpel ?>" <?php
                if ($row->id_matpel == $examData->id_matpel) {
                    echo "selected";
                }
                ?>><?php echo $row->nama_matpel ?></option>
            <?php
            endforeach;
            ?>
        </select> <br/>
        <input type="checkbox" class="checkbox" name="" id="new-matpel">
        <label for="new-matpel">New Subject</label> <br/>
        <input class="input-exam custom-input" type="text" name="nama_matpel" id="nama_matpel" placeholder="Subject" required disabled>
    </div>
    <div class="form-froup">
        <label for="waktu_menit">Time limit (minute)</label>
        <input class="input-exam custom-input" type="number" min="1" name="waktu_menit" id="waktu_menit" value="<?= $examData->waktu_menit ?>" required>
    </div>

    <?php
    $i = 0;
    foreach ($questionData as $k => $v) :
    ?>
        <div class="quest-section">
            <div class="title">Question Number <?= $i+1 ?></div>
            <div class="form-group">
                <textarea class="input-exam custom-input" name="pertanyaan[<?= $v->id_pertanyaan ?>]" id="pertanyaan<?= $v->id_pertanyaan ?>" cols="30" rows="10" required><?= $v->pertanyaan ?></textarea>
            </div>
            <div class="ans-section">
                <?php
                $j = 0;
                foreach ($answerData as $key => $value) :
                ?>
                    <div class="form-group">
                        <input type="radio" class="radiobox input-exam" name="benar[<?= $i ?>]" id="benar<?= $value->id_pilihan ?>" value="<?= $value->id_pilihan ?>" <?php
                        if ($value->benar) {
                            echo "checked";
                        }
                        ?> required>
                        <label for="benar<?= $value->id_pilihan ?>"></label>
                    </div>
                    <textarea  class="input-exam custom-input" name="jawaban[<?= $value->id_pilihan ?>]" id="jawaban<?= $value->id_pilihan ?>" cols="30" rows="10" required><?= $value->jawaban ?></textarea>
                <?php
                    array_splice($answerData, 0, 1);
                    $j++;
                    if ($j == $numAns) {
                        break;
                    }
                endforeach;
                $i++;
                ?>
            </div>
        </div>
    <?php
    endforeach;
    ?>
    <button type="submit" id="save-btn" class="btn green round">Save</button>
</form>

<div class="loader-make" id="loader-make">
    <div class="spin-container">
        <div class="spin"></div>
    </div>
    <div class="message">Uploading Content...</div>
</div>

<script>
$("#new-matpel").change((event) => {
    if ($(event.currentTarget).prop('checked')) {
        $("#id_matpel").prop('disabled', true);
        $("#nama_matpel").prop('disabled', false);
    }
    else {
        $("#nama_matpel").prop('disabled', true);
        $("#id_matpel").prop('disabled', false);
    }
});

$("#editexam-form").submit((event) => {
    let inputFill = () => {
        let inputData = $(".input-exam:not(:disabled)");

        for (let i = 0; i < inputData.length; i++) {
            let isFilled = ($(inputData[i]).val() === "" || $(inputData[i]).val() === null)? false : true;
            if (!isFilled) return false;
        }
        return true;
    }

    if (inputFill()) {
        event.preventDefault();
        $("#fragment").scrollTop(0);

        $("#save-btn").prop("disabled", true);

        $("#loader-make").css("display", "grid");

        let targetUrl = $("#editexam-form").attr('action');
        // console.log(targetUrl);
        let formData = new FormData($("#editexam-form")[0]);

        $.ajax({
            type: 'post',
            url: targetUrl,
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: (data, textStatus, jqHXR) => {
                // console.log(data);
                beginTransaction('/'+control+'/getpage/exam');
            },
            error: (data, textStatus, jqHXR) => {
                // console.log(data);
                $("#loader-make").css("display", "none");
                $("#save-btn").prop("disabled", false);
            } 
        });

        
    }
});
</script>
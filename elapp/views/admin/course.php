<h2 class="title">Course</h2>

<nav class="inner-nav" id="control-nav" navtype="adminnav">
    <div class="btn round green nav-link newcourse-link" target-page="newcourse" direct=""><i class="fas fa-plus-circle fa-fw"></i> New Course</div>
</nav>

<table id="course-table" class="display">
    <thead>
        <tr>
            <th>Course ID</th>
            <th>Grade</th>
            <th>Subject</th>
            <th>Course Title</th>
            <!-- <th>File Name</th> -->
            <th>Upload Date</th>
            <th>Update Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($courseList as $key => $value) :
        ?>
        <tr>
            <td><?= $value->id_materi ?></td>
            <td><?= $value->kelas ?></td>
            <td><?= $value->nama_matpel ?></td>
            <td><?= $value->nama_materi ?></td>
            <!-- <td><?= $value->nama_file ?></td> -->
            <td><?= $value->tanggal_upload ?></td>
            <td><?= $value->tanggal_update ?></td>
            <td>
                <div class="btn-group">
                    <div class="action-btn yellow newcourse-link" target-page="editcourse/<?= $value->id_materi ?>" direct=""><i class="fas fa-edit fa-fw"></i></div>
                    <div class="action-btn grey"><i class="fas fa-trash-alt fa-fw"></i></div>
                    <div class="action-btn blue newcourse-link" target-page="relate/<?= $value->id_materi ?>" direct=""><i class="fas fa-link fa-fw"></i></div>
                    <div class="action-btn red newcourse-link" target-page="<?= base_url("pdfviewer?file=").base_url("uploads/pdf/$value->nama_file") ?>" direct="1" blank="1"><i class="fas fa-file-pdf fa-fw"></i></div>
                </div>
            </td>
        </tr>
        <?php
        endforeach;
        ?>
    </tbody>
</table>

<script>
$(".newcourse-link").click((event) => {
    clickEvent(event, control, false);
});

$("#course-table").DataTable();
</script>
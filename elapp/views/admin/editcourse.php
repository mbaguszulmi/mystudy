<?php
$grades = array(
    1 => '1 SD',
    2 => '2 SD',
    3 => '3 SD',
    4 => '4 SD',
    5 => '5 SD',
    6 => '6 SD',
    7 => '1 SMP',
    8 => '2 SMP',
    9 => '3 SMP',
);
?>

<h2 class="title">Edit Course</h2>

<form action="/admin/updatecourse" id="editcourse-form" class="custom-form" enctype="multipart/form-data" method="post">
    <input type="hidden" name="id_materi" value="<?= $courseData->id_materi ?>">
    <div class="form-group">
        <input class="input-data custom-input" type="text" name="nama_materi" id="nama_materi" value="<?= $courseData->nama_materi ?>" placeholder="Course Title" required>
    </div>
    <div class="form-group">
        
        <select class="input-data custom-input" name="kelas" id="kelas" required>
            <option value="1" disabled>Choose Grade</option>
            <?php
            foreach ($grades as $key => $value):
            ?>
                <option value="<?= $key ?>" <?php
                    if ($key == (int)$courseData->kelas) {
                        echo "selected";
                    }
                ?>><?= $value ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </div>
    <div class="form-group">
        <select class="input-data custom-input" name="id_matpel" id="id_matpel" required>
            <option value="1" disabled>Choose Subject</option>
            <?php
            foreach ($matpelList as $key => $row):
            ?>
                <option value="<?php echo $row->id_matpel ?>" <?php
                    if ($row->id_matpel == (int)$courseData->id_matpel) {
                        echo "selected";
                    }
                ?>><?php echo $row->nama_matpel ?></option>
            <?php
            endforeach;
            ?>
        </select> <br/>
        <input type="checkbox" class="checkbox" name="" id="new-matpel">
        <label for="new-matpel">New Subject</label>
        <input class="input-data custom-input" type="text" name="nama_matpel" id="nama_matpel" placeholder="Subject" required disabled>
    </div>
    <div class="form-group">
        <input type="checkbox" class="checkbox" name="" id="edit-file">
        <label for="edit-file">Edit Course File</label>
        <input type="hidden" name="id_download" value="<?= $courseData->id_download ?>">
        <input type="hidden" name="update_file" id="update_file" value="0">
        <input class="input-data custom-input" type="file" name="file_materi" id="file_materi" required disabled>
    </div>
    <div class="form-group">
        <button type="submit" id="submit-btn" class="btn green round">Save Course</button>
    </div>
</form>

<div class="loader-make" id="loader-make">
    <div class="spin-container">
        <div class="spin"></div>
    </div>
    <div class="message">Uploading Content...</div>
</div>

<script>
$("#new-matpel").change((event) => {
    if ($(event.currentTarget).prop('checked')) {
        $("#id_matpel").prop('disabled', true);
        $("#nama_matpel").prop('disabled', false);
    }
    else {
        $("#nama_matpel").prop('disabled', true);
        $("#id_matpel").prop('disabled', false);
    }
});

// console.log($("#file_materi"));

$("#edit-file").change((event) => {
    if ($(event.currentTarget).prop('checked')) {
        $("#file_materi").prop('disabled', false);
        $("#update_file").val("1");
    }
    else {
        $("#file_materi").val("");
        $("#update_file").val("0");
        $("#file_materi").prop('disabled', true);
    }
});

$("#submit-btn").click((event) => {
    let inputFill = () => {
        let inputData = $(".input-data:not(:disabled)");

        for (let i = 0; i < inputData.length; i++) {
            let isFilled = ($(inputData[i]).val() === "" || $(inputData[i]).val() === null)? false : true;
            if (!isFilled) return false;
        }
        return true;
    }

    if (inputFill()) {
        event.preventDefault();
        $(event.currentTarget).prop("disabled", true);
        $("#loader-make").css("display", "grid");

        let targetUrl = $("#editcourse-form").attr('action');
        // console.log(targetUrl);
        let formData = new FormData($("#editcourse-form")[0]);

        $.ajax({
            type: 'post',
            url: targetUrl,
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: (data, textStatus, jqHXR) => {
                console.log(data);
                beginTransaction('/'+control+'/getpage/course');
            },
            error: (data, textStatus, jqHXR) => {
                console.log(data);
                $("#loader-make").css("display", "none");
                $(event.currentTarget).prop("disabled", false);
            } 
        });
    }
    else {
        $(event.currentTarget).prop("disabled", false);
    }
});
</script>
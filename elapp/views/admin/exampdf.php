<?php
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle($examData->nama_soal);
$pdf->SetHeaderMargin(15);
// $pdf->SetTopMargin(50);
$pdf->SetMargins(25, 43, 25, true);
$pdf->setFooterMargin(20);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('MyStudy');
$pdf->SetDisplayMode('real', 'default');
$pdf->SetFont('helvetica', '', 12);
$pdf->setHeaderContent($examData->nama_soal, $examData->nama_matpel, (int)$examData->kelas, substr($examData->tanggal_upload, 0, 4));
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->AddPage();

// $pdf->Write(5, 'Some sample text');
// $html = '
// <p>Lorem ipsum dolo© r sit amet consectetur, adipisicing elit. Quis eaque cupiditate veniam nam, reiciendis temporibus maxime totam provident neque aliquid, aut consectetur, explicabo ipsa inventore quod quia nisi impedit. Quaerat!</p>
// <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio aliquam a eum officia! Officia voluptate repellendus quisquam magni repudiandae iure ullam. Necessitatibus sint animi vero ab! Adipisci sit vero maiores.</p>
// <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia quis beatae ipsum ipsa amet pariatur! Vero, labore, nostrum autem iure dolores veritatis repellendus, commodi magnam illo ea architecto itaque. Repellat.</p>
// <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga doloremque eius a numquam quaerat, quibusdam, animi iure nostrum ullam non totam corporis laudantium natus porro possimus! Iste ullam blanditiis voluptatem?</p>
// <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, doloribus quia sed error consectetur, totam esse, soluta qui nisi corporis autem ad repudiandae nesciunt obcaecati pariatur possimus reprehenderit molestiae placeat?</p>
// <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum at explicabo vitae quis fugit excepturi dicta architecto reiciendis, sed harum quas quod doloremque ea alias, consequuntur possimus aut quam minima.</p>
// <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Excepturi, ut in! Quas ullam repellat enim voluptatum numquam. Fugit voluptas asperiores molestiae reprehenderit dolorum sunt quae nesciunt et sit. Minima, aperiam?</p>
// <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Enim optio, quasi magni necessitatibus, eaque qui dolorem odio magnam ad laboriosam fugiat quis ea perspiciatis quidem sed molestias doloribus natus ipsa.</p>
// <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloremque eaque nihil natus quia laboriosam facere similique, aut dolorem illum dignissimos tempore expedita veniam. Magni, sequi cumque vel sint quis numquam!</p>
// <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem totam porro deleniti perspiciatis corporis? Sapiente necessitatibus exercitationem hic ipsam dolor aliquam quod nisi, pariatur doloribus eum atque nam dolorum itaque.</p>
// <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi, laudantium similique nemo explicabo dolore quia iusto amet eos modi, eum at qui blanditiis! Ullam nulla mollitia perferendis quia asperiores repellat!</p>
// <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa minima dicta reprehenderit! Non quae aspernatur asperiores labore, suscipit dicta soluta, similique cumque at sit a officia veniam unde provident consectetur.</p>
// <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim repellendus quam placeat magni voluptates distinctio? At sit magnam quo deleniti ullam aliquid distinctio quas tempora? In excepturi dicta quo corporis?</p>
// <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius corrupti quia perferendis atque laudantium suscipit officia sint blanditiis maxime! Optio quam, quis commodi aliquam odit deleniti ex nemo ut dignissimos.</p>
// ';

// $data = array(
//     '<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quis eaque cupiditate veniam nam, reiciendis temporibus maxime totam provident neque aliquid, aut consectetur, explicabo ipsa inventore quod quia nisi impedit. Quaerat!</p>',
//     '<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio aliquam a eum officia! Officia voluptate repellendus quisquam magni repudiandae iure ullam. Necessitatibus sint animi vero ab! Adipisci sit vero maiores.</p>',
//     '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia quis beatae ipsum ipsa amet pariatur! Vero, labore, nostrum autem iure dolores veritatis repellendus, commodi magnam illo ea architecto itaque. Repellat.</p>',
//     '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga doloremque eius a numquam quaerat, quibusdam, animi iure nostrum ullam non totam corporis laudantium natus porro possimus! Iste ullam blanditiis voluptatem?</p>',
//     '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, doloribus quia sed error consectetur, totam esse, soluta qui nisi corporis autem ad repudiandae nesciunt obcaecati pariatur possimus reprehenderit molestiae placeat?</p>',
//     '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum at explicabo vitae quis fugit excepturi dicta architecto reiciendis, sed harum quas quod doloremque ea alias, consequuntur possimus aut quam minima.</p>',
//     '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Excepturi, ut in! Quas ullam repellat enim voluptatum numquam. Fugit voluptas asperiores molestiae reprehenderit dolorum sunt quae nesciunt et sit. Minima, aperiam?</p>',
//     '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Enim optio, quasi magni necessitatibus, eaque qui dolorem odio magnam ad laboriosam fugiat quis ea perspiciatis quidem sed molestias doloribus natus ipsa.</p>',
//     '<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloremque eaque nihil natus quia laboriosam facere similique, aut dolorem illum dignissimos tempore expedita veniam. Magni, sequi cumque vel sint quis numquam!</p>',
//     '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem totam porro deleniti perspiciatis corporis? Sapiente necessitatibus exercitationem hic ipsam dolor aliquam quod nisi, pariatur doloribus eum atque nam dolorum itaque.</p>',
//     '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi, laudantium similique nemo explicabo dolore quia iusto amet eos modi, eum at qui blanditiis! Ullam nulla mollitia perferendis quia asperiores repellat!</p>',
//     '<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa minima dicta reprehenderit! Non quae aspernatur asperiores labore, suscipit dicta soluta, similique cumque at sit a officia veniam unde provident consectetur.</p>',
//     '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim repellendus quam placeat magni voluptates distinctio? At sit magnam quo deleniti ullam aliquid distinctio quas tempora? In excepturi dicta quo corporis?</p>',
//     '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius corrupti quia perferendis atque laudantium suscipit officia sint blanditiis maxime! Optio quam, quis commodi aliquam odit deleniti ex nemo ut dignissimos.</p>'
// );

$html = '<ol style="text-align: justify">';
// for ($i=1; $i <= 20; $i++) { 
//     $html .= '<li>'.$i.'</li>';
// }
$correct = 'style="background-color: yellow"';

foreach ($questionData as $key => $value) {
    $html .= '<li>'.$value->pertanyaan;

    $i = 0;
    $html .= '<ol type="A">';
    foreach ($answerData as $k => $v) {
        $html .= '<li ';
        if ($v->benar == "1") {
            $html .= $correct;
        }
        $html .= '>'.$v->jawaban.'</li>';

        array_splice($answerData, 0, 1);
        $i++;
        if ($i == $numAns) {
            break;
        }
    }
    $html .= '</ol>';
    $html .= '</li>';

}
$html .= '</ol>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->Output(substr(str_replace(" ", "-", $examData->nama_soal), 0, 15).'.pdf', 'I');
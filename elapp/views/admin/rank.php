<h2 class="title">Students Rank</h2>

<div class="elementary">
    <div class="title">Elementary School</div>

    <table id="elementary-rank" class="display">
        <thead>
            <tr>
                <th>#</th>
                <th>Student ID</th>
                <th>Name</th>
                <th>Grade</th>
                <th>Points</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($elementaryRank as $key => $value) :
            ?>
                <tr>
                    <td><?= $i++ ?></td>
                    <td><?= $value->id_siswa ?></td>
                    <td><?= $value->nama ?></td>
                    <td><?= $value->kelas ?></td>
                    <td><?= $value->total_poin ?></td>
                </tr>
            <?php
            endforeach;
            ?>
        </tbody>
    </table>
</div>

<div class="junior">
    <div class="title">Junior High School</div>

    <table id="junior-rank" class="display">
        <thead>
            <tr>
                <th>#</th>
                <th>Student ID</th>
                <th>Name</th>
                <th>Grade</th>
                <th>Points</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($juniorRank as $key => $value) :
            ?>
                <tr>
                    <td><?= $i++ ?></td>
                    <td><?= $value->id_siswa ?></td>
                    <td><?= $value->nama ?></td>
                    <td><?= $value->kelas ?></td>
                    <td><?= $value->total_poin ?></td>
                </tr>
            <?php
            endforeach;
            ?>
        </tbody>
    </table>
</div>

<script>
$("#elementary-rank").DataTable();
$("#junior-rank").DataTable();
</script>
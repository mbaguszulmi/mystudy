<h2 class="title">Dashboard</h2>

<div class="summary">
    <div class="numstudent summary-section">
        <div class="title"><i class="fas fa-user-alt fa-fw"></i> Students</div>
        <div class="content">
            <?= $numStudent ?>
        </div>
    </div>
    <div class="numcourse summary-section">
        <div class="title"><i class="fas fa-book fa-fw"></i> Courses</div>
        <div class="content">
            <?= $numCourse ?>
        </div>
    </div>
    <div class="numexam summary-section">
        <div class="title"><i class="fas fa-tasks fa-fw"></i> Exams</div>
        <div class="content">
            <?= $numExam ?>
        </div>
    </div>
</div>

<div class="stats">
    <div class="user-stats"></div>
    <div class="exam-stats"></div>
</div>
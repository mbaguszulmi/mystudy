<h2 class="title">Relate Course And Exam</h2>

<?php
if (count($examList) > 0) :
?>
Relate <?= $courseData->nama_materi ?> <br/>
In <?= $matpel->nama_matpel ?> <br/>
Grade <?= $courseData->kelas ?> <br/>
With 
<form action="<?php
    if ($numRelate > 0) {
        echo "/admin/updaterelate";
    }
    else {
        echo "/admin/addrelate";
    }
?>" method="post" class="custom-form" id="relate-form">
    <input type="hidden" name="id_materi" value="<?= $courseData->id_materi ?>">
    <div class="form-group">
        <select name="id_soal" id="id_soal" required>
            <option value="" disabled <?php
                if ($numRelate <= 0) {
                    echo "selected";
                }
            ?>>Select exam</option>
            <?php
            foreach ($examList as $key => $value) :
            ?>
                <option value="<?= $value->id_soal ?>" 
                <?php
                if ($numRelate > 0 && $relateData->id_soal == $value->id_soal) {
                    echo "selected";
                }
                ?>><?= $value->nama_soal ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </div>

    <button type="submit" id="submit-btn" class="btn green round">Save</button>
</form>
<?php
else :
    echo "There's no exam found in ".$matpel->nama_matpel." subject with grade ".$courseData->kelas.".";
endif;
?>

<div class="loader-make" id="loader-make">
    <div class="spin-container">
        <div class="spin"></div>
    </div>
    <div class="message">Uploading Content...</div>
</div>

<script>
$("#relate-form").submit((event) => {
    let inputFill = () => {
        // let inputData = $(".input-exam:not(:disabled)");

        // for (let i = 0; i < inputData.length; i++) {
        //     let isFilled = ($(inputData[i]).val() === "" || $(inputData[i]).val() === null)? false : true;
        //     if (!isFilled) return false;
        // }

        if ($("#id_soal").val() === '') {
            return false;
        }
        return true;
    }

    if (inputFill()) {
        event.preventDefault();
        $("#fragment").scrollTop(0);

        $("#submit-btn").prop("disabled", true);

        $("#loader-make").css("display", "grid");

        let targetUrl = $("#relate-form").attr('action');
        console.log(targetUrl);
        let formData = new FormData($("#relate-form")[0]);

        $.ajax({
            type: 'post',
            url: targetUrl,
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: (data, textStatus, jqHXR) => {
                console.log(data);
                beginTransaction('/'+control+'/getpage/course');
            },
            error: (data, textStatus, jqHXR) => {
                console.log(data);
                $("#loader-make").css("display", "none");
                $("#submit-btn").prop("disabled", false);
            } 
        });

        
    }
});
</script>
<h2 class="title">Exam</h2>

<table id="exam-table" class="display">
    <thead>
        <tr>
            <th>Subject</th>
            <th>Exam Title</th>
            <th>Time Limit</th>
            <th>Points</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($examList as $key => $value) :
        ?>
        <tr>
            <td><?= $value->nama_matpel ?></td>
            <td><?= $value->nama_soal ?></td>
            <td><?= $value->waktu_menit ?></td>
            <td>-</td>
            <td>
            <div class="btn-group">
                    <div class="action-btn blue exam-link" target-page="examroom/<?= $value->id_soal ?>" direct=""><i class="fas fa-play-circle fa-fw"></i></div>
                </div>
            </td>
        </tr>
        <?php
        endforeach;
        ?>
    </tbody>
</table>

<h2 class="title">Exam Result</h2>

<table id="result-table" class="display">
    <thead>
        <tr>
            <th>Date</th>
            <th>Exam Title</th>
            <th>Subject</th>
            <th>Points</th>
            <th>True</th>
            <th>False</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($examResult as $key => $value) :
        ?>
            <tr>
                <td><?= $value->tanggal ?></td>
                <td><?= $value->nama_soal ?></td>
                <td><?= $value->nama_matpel ?></td>
                <td><?= round($value->poin, 2) ?></td>
                <td><?= $value->benar ?></td>
                <td><?= $value->salah ?></td>
                <td>
                    <div class="action-btn red exam-link" target-page="<?= base_url("pdfviewer?file=").base_url("dashboard/getpage/exampdf/$value->id_soal") ?>" direct="1" blank="1"><i class="fas fa-file-pdf fa-fw"></i></div>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
    </tbody>
</table>

<script>
$(".exam-link").click((event) => {
    clickEvent(event, control, false);
});
$("#exam-table").DataTable();
$("#result-table").DataTable();
</script>
<div class="signup-container">
    <div class="form-container">
        <div class="title-section">
            <a href="<?= base_url() ?>"><img src="/assets/images/logo.png" alt="<?= $site['name'] ?>"></a>
            <h1 class="title">Sign Up for Student</h1>
        </div>

        <form action="<?= base_url("dashboard/signingup") ?>" class="signup-form" method="post">
            <div class="form-group name">
                <input autofocus type="text" name="nama" id="nama" class="input" placeholder="Name" required>
            </div>
            <div class="form-group school">
                <input type="text" name="asal_sekolah" id="asal_sekolah" class="input" placeholder="School" required>
            </div>
            <div class="form-group grade">
                <select name="kelas" id="kelas" class="input" required>
                    <option value="" disabled selected>Choose grade</option>
                    <option value="1">1 SD</option>
                    <option value="2">2 SD</option>
                    <option value="3">3 SD</option>
                    <option value="4">4 SD</option>
                    <option value="5">5 SD</option>
                    <option value="6">6 SD</option>
                    <option value="7">7 SMP</option>
                    <option value="8">8 SMP</option>
                    <option value="9">9 SMP</option>
                </select>
            </div>
            <div class="radio-group">
                <input class="radiobox" type="radio" name="jenis_kelamin" id="jenis_kelamin1" value="0" required>
                <label for="jenis_kelamin1">Male</label>
                <input class="radiobox" type="radio" name="jenis_kelamin" id="jenis_kelamin2" value="1" required>
                <label for="jenis_kelamin2">Female</label>
            </div>
            <div class="form-group birthdate">
                <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="input" required>
            </div>
            <div class="form-group address">
                <input type="text" name="alamat" id="alamat" class="input" placeholder="Address" required>
            </div>
            <div class="form-group phone">
                <input type="tel" name="no_hp" id="no_hp" pattern="[0-9].{0,12}" class="input" placeholder="Phone number" required>
            </div>
            <div class="form-group email">
                <input type="email" name="email" id="email" placeholder="Email" class="input" required>
            </div>
            <div class="form-group password">
                <input type="password" pattern="[A-Za-z0-9.,@!#$%&*].{7,}" title="A-Z or a-z or 0-9 or .,@!#$%&*" name="password" id="password" class="input" placeholder="Password" required>
            </div>
            
            <div class="submit-area">
                <button type="submit" class="btn green round"><i class="fas fa-user-plus fa-fw"></i> Sign Up</button>
            </div>
        </form>
    </div>
</div>
<?php
$grades = array(
    1 => '1 SD',
    2 => '2 SD',
    3 => '3 SD',
    4 => '4 SD',
    5 => '5 SD',
    6 => '6 SD',
    7 => '1 SMP',
    8 => '2 SMP',
    9 => '3 SMP',
);
?>

<h2 class="title">Account Settings</h2>

<form id="profile-form" class="custom-form" action="<?= base_url("dashboard/updateprofile") ?>" method="post">
    <div class="form-group name">
        <input value="<?= $userData->nama ?>" autofocus type="text" name="nama" id="nama" class="input input-check custom-input" placeholder="Name" required>
    </div>
    <div class="form-group school">
        <input value="<?= $userData->asal_sekolah ?>" type="text" name="asal_sekolah" id="asal_sekolah" class="input input-check custom-input" placeholder="School" required>
    </div>
    <div class="form-group grade">
        <select name="kelas" id="kelas" class="input input-check custom-input" required>
            <option value="" disabled selected>Choose grade</option>
            <?php
            foreach ($grades as $key => $value):
            ?>
                <option value="<?= $key ?>" <?php
                    if ($key == (int)$userData->kelas) {
                        echo "selected";
                    }
                ?>><?= $value ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </div>
    <div class="radio-group">
        <input class="radiobox" type="radio" name="jenis_kelamin" id="jenis_kelamin1" value="0" <?php
        if ($userData->jenis_kelamin == '0') {
            echo 'checked';
        }
        ?> required>
        <label for="jenis_kelamin1">Male</label>
        <input class="radiobox" type="radio" name="jenis_kelamin" id="jenis_kelamin2" value="1" <?php
        if ($userData->jenis_kelamin == '1') {
            echo 'checked';
        }
        ?> required>
        <label for="jenis_kelamin2">Female</label>
    </div>
    <div class="form-group birthdate">
        <label for="tanggal_lahir">Birth Date</label>
        <input value="<?= $userData->tanggal_lahir ?>" type="date" name="tanggal_lahir" id="tanggal_lahir" class="input input-check custom-input" required>
    </div>
    <div class="form-group address">
        <input value="<?= $userData->alamat ?>" type="text" name="alamat" id="alamat" class="input input-check custom-input" placeholder="Address" required>
    </div>
    <div class="form-group phone">
        <input value="<?= $userData->no_hp ?>" type="tel" name="no_hp" id="no_hp" pattern="[0-9].{0,12}" class="input input-check custom-input" placeholder="Phone number" required>
    </div>
    <div class="form-group email">
        <input value="<?= $userData->email ?>" type="email" name="email" id="email" placeholder="Email" class="input input-check custom-input" required>
    </div>
    <div class="form-group password">
        <input type="checkbox" name="update_password" id="update_password" class="checkbox">
        <label for="update_password">Update password</label>
        <input type="password" pattern="[A-Za-z0-9.,@!#$%&*].{8,}" title="A-Z or a-z or 0-9 or .,@!#$%&*" name="password" id="password" class="input input-check custom-input" placeholder="Password" disabled required>
    </div>

    <div class="form-group">
        <input type="password" name="confirm" id="confirm" class="input-check custom-input" placeholder="Type old password to update" required>
    </div>
    
    <div class="submit-area">
        <button type="submit" id="submit-btn" class="btn green round" disabled><i class="fas fa-save fa-fw"></i> Save</button>
    </div>
</form>

<div class="loader-make" id="loader-make">
    <div class="spin-container">
        <div class="spin"></div>
    </div>
    <div class="message">Updating profile...</div>
</div>

<script src="<?= base_url("assets/js/sha1.min.js") ?>"></script>
<script>
    $(document).ready(() => {
        let curPass = '<?= $curPass ?>';
        
        $("#update_password").change((event) => {
            // console.log("change");
            if ($(event.currentTarget).prop("checked")) {
                $("#password").prop("disabled", false);
            }
            else {
                $("#password").prop("disabled", true);
            }
        });

        let checkPass = () => {
            let pass = $("#confirm").val();

            if (curPass === sha1(pass)) {
                $("#submit-btn").prop("disabled", false);
                return true;
            }
            else {
                $("#submit-btn").prop("disabled", true);
                return false;
            }
        }

        $("#confirm").keyup((event) => {
            checkPass();
        });

        $("#profile-form").submit((event) => {
            event.preventDefault();
            // let test = true;

            if (!checkPass()) {
                $("#confirm").val();
            }
            else {
                $("#submit-btn").prop("disabled", true);

                let targetUrl = $(event.currentTarget).attr('action');
                // console.log(targetUrl);
                let formData = new FormData($(event.currentTarget)[0]);
                $("#loader-make").css("display", "grid");

                $.ajax({
                    type: 'post',
                    url: targetUrl,
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: (data, textStatus, jqHXR) => {
                        console.log(data);
                        $(".sidenav .nav-link.active").removeClass("active");
                        $(".sidenav .nav-link[target-page='index']").addClass("active");

                        normalizeAnimation();
                        // beginTransaction('/'+control+'/getpage/index');
                        window.location = '<?= base_url("dashboard") ?>';
                    },
                    error: (data, textStatus, jqHXR) => {
                        console.log(data);
                        $("#loader-make").css("display", "none");
                        $(event.currentTarget).prop("disabled", false);
                    } 
                });
            }
        });
    });
</script>

<?php
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle($examData->nama_soal);
$pdf->SetHeaderMargin(15);
$pdf->SetMargins(25, 43, 25, true);
$pdf->setFooterMargin(20);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('MyStudy');
$pdf->SetDisplayMode('real', 'default');
$pdf->SetFont('helvetica', '', 12);
$pdf->setHeaderContent($examData->nama_soal, $examData->nama_matpel, (int)$examData->kelas, substr($examData->tanggal_upload, 0, 4));
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->AddPage();

$html = '<ol style="text-align: justify">';
$correct = 'style="background-color: yellow"';

foreach ($questionData as $key => $value) {
    $html .= '<li>'.$value->pertanyaan;

    $i = 0;
    $html .= '<ol type="A">';
    foreach ($answerData as $k => $v) {
        $html .= '<li ';
        if ($v->benar == "1") {
            $html .= $correct;
        }
        $html .= '>'.$v->jawaban.'</li>';

        array_splice($answerData, 0, 1);
        $i++;
        if ($i == $numAns) {
            break;
        }
    }
    $html .= '</ol>';
    $html .= '</li>';

}
$html .= '</ol>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->Output(substr(str_replace(" ", "-", $examData->nama_soal), 0, 15).'.pdf', 'I');
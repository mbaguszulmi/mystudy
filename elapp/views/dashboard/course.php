<h2 class="title">Course</h2>

<table id="course-table" class="display">
    <thead>
        <tr>
            <th>Subject</th>
            <th>Course Title</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($courseList as $key => $value) :
        ?>
        <tr>
            <td><?= $value->nama_matpel ?></td>
            <td><?= $value->nama_materi ?></td>
            <td><?= $value->tanggal_upload ?></td>
            <td>
                <div class="btn-group">
                    <div class="action-btn red course-link" target-page="<?= base_url("dashboard/readCourse/$value->id_materi") ?>" direct="1" blank="1"><i class="fas fa-file-pdf fa-fw"></i></div>
                </div>
            </td>
        </tr>
        <?php
        endforeach;
        ?>
    </tbody>
</table>

<script>
$(".course-link").click((event) => {
    clickEvent(event, control, false);
});

$("#course-table").DataTable();
</script>
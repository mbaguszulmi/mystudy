<h2 class="title">Dashboard</h2>

<div class="summary">
    <div class="rank summary-section">
        <div class="title"><i class="fas fa-user-alt fa-fw"></i> Rank</div>
        <div class="content">
            Your rank : <?= $rank ?>
        </div>
    </div>
    <div class="course-read summary-section">
        <div class="title"><i class="fas fa-book fa-fw"></i> Course has ben read</div>
        <div class="content">
            <?= $numRead ?>
        </div>
    </div>
    <div class="exam-finished summary-section">
        <div class="title"><i class="fas fa-tasks fa-fw"></i> Exam finished</div>
        <div class="content">
            <?= $numExam ?>
        </div>
    </div>
</div>
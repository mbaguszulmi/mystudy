<h2 class="title">Exam Room</h2>

<div class="exam-container">
    <div class="exam-header">
        <div class="navigation">
            <div class="go-to" id="go-to">1/40</div>
            <div class="exam-map" id="exam-map"></div>
            <div class="action-btn lgrey" id="prev-btn"><i class="fas fa-chevron-circle-left fa-fw"></i></div>
            <div class="action-btn lgrey" id="next-btn"><i class="fas fa-chevron-circle-right fa-fw"></i></div>
            <div class="action-btn lgrey" id="bookmark"><i class="far fa-bookmark fa-fw"></i></div>
        </div>
        <div class="detail">
            <span id="exam-title"></span>
            <span id="exam-subject"></span>
            <span id="exam-grade"></span>
        </div>
        <div class="action-btn lgrey" id="submit-btn"><i class="fas fa-paper-plane fa-fw"></i></div>
        <div class="countdown">
            <i class="far fa-clock fa-fw"></i>
            <span class="timer" id="timer">50:43</span>
        </div>
    </div>
    <div class="exam-content">
        <div class="question-section" id="question-section"></div>
        <div class="answer-section" id="answer-section"></div>
    </div>
</div>

<div class="exam-result" id="exam-result">
    <div class="exam-title"><i class="fas fa-tasks fa-fw"></i> <?= $examData['nama_soal'] ?></div>
    <div class="exam-subject"><i class="fas fa-tag fa-fw"></i> <?= $examData['nama_matpel'] ?></div>
    <div class="exam-grade"><i class="fas fa-graduation-cap fa-fw"></i> <?= $examData['kelas'] ?></div>
    <div class="result"><i class="fasfa-poll fa-fw"></i> <span id="result">80</span></div>
    <div class="detail">
        <i class="fas fa-check-circle fa-fw"></i> <span class="correctAns" id="correctAns">3</span>
        <i class="fas fa-times-circle fa-fw"></i> <span class="falseAns" id="falseAns">3</span>
    </div>

    <div class="button-group">
        <div class="btn green round exam-link" target-page="exam" direct="" id="to-exam"><i class="fas fa-arrow-circle-left fa-fw"></i> Back to exam page</div>
        <div class="btn red round exam-link" target-page="<?= base_url("pdfviewer?file=").base_url("dashboard/getpage/exampdf/").$examData['id_soal'] ?>" direct="1" blank="1" id="download"><i class="fas fa-download fa-fw"></i> Download This PDF</div>
    </div>
</div>

<div class="loader-make" id="loader-make">
    <div class="spin-container">
        <div class="spin"></div>
    </div>
    <div class="message">Submitting exam...</div>
</div>

<script>

$(document).ready(() => {
    let currentPos      = 0;
    let userAnswerData  = [];
    let endTimer        = 0;
    let offsetHour      = 0;

    // Getting data
    let examData        = <?= json_encode($examData) ?>;
    let questionData    = <?= json_encode($questionData) ?>;
    let answerData      = <?= json_encode($answerData) ?>;
    let numAns          = <?= $numAns ?>;
    let numQuest        = questionData.length;

    let ansCount = 0;
    examData.pertanyaanArr = questionData;
    examData.pertanyaanArr.forEach(q => {
        q.jawaban = [];

        let i;
        for (i = ansCount; i < ansCount+numAns; i++) {
            const a = answerData[i];
            q.jawaban.push(a);
        }
        ansCount = i;

        answerJson = {
            answerId: 0,
            bookmark: false
        };
        userAnswerData.push(answerJson);
    });

    console.log(examData, userAnswerData);
    // console.log(questionData);
    // console.log(answerData);
    // console.log(numAns);

    // Setup exam
    let setAnswer = (idx, value) => {
        userAnswerData[idx].answerId = value;
    }

    let toggleBookmark = (idx) => {
        userAnswerData[idx].bookmark = !userAnswerData[idx].bookmark;

        if (userAnswerData[idx].bookmark) {
            $("#bookmark").html(`<i class="fas fa-bookmark fa-fw"></i>`);
            $($("#exam-map .number")[idx]).addClass("marked");
        } else {
            $("#bookmark").html(`<i class="far fa-bookmark fa-fw"></i>`);
            $($("#exam-map .number")[idx]).removeClass("marked");
        }
    }

    let setExamQuestion = (idx) => {
        $("#go-to").html(`${currentPos+1}/${numQuest}`);
        $("#question-section").html(examData.pertanyaanArr[idx].pertanyaan);

        $("#exam-map .number.current").removeClass("current");
        $($("#exam-map .number")[idx]).addClass("current");

        $("#answer-section").empty();
        let i = 0
        examData.pertanyaanArr[idx].jawaban.forEach(a => {
            let checked = '';
            if (parseInt(a.id_pilihan) == userAnswerData[idx].answerId) {
                checked = ' checked';
            }

            $("#answer-section").append(`
            <div class="form-group">
                <input type="radio" class="radiobox" name="answer" id="answer${i}" value="${a.id_pilihan}" ${checked} required>
                <label for="answer${i++}"></label>
            </div>
            <div class="answer-content">${a.jawaban}</div>
            `);
        });

        if (userAnswerData[idx].bookmark) {
            $("#bookmark").html(`<i class="fas fa-bookmark fa-fw"></i>`);
        }
        else {
            $("#bookmark").html(`<i class="far fa-bookmark fa-fw"></i>`);
        }

        $("input[name='answer']").change((event) => {
            setAnswer(idx, parseInt($("input[name='answer']:checked").val()));
            $($("#exam-map .number")[idx]).addClass("filled");
        });
    }

    let setupExam = () => {
        // $("#go-to").html(`${currentPos+1}/${numQuest}`);

        for (let i = 1; i <= numQuest; i++) {
            $("#exam-map").append(`<div class="number">${i}</div>`)
        }
        $(".number").first().addClass("current");

        $("#exam-title").html(`<i class="fas fa-tasks fa-fw"></i> ${examData.nama_soal}`);
        $("#exam-subject").html(`<i class="fas fa-tag fa-fw"></i> ${examData.nama_matpel}`);

        let suffix = 'SD';
        let grade = parseInt(examData.kelas);
        if (grade >= 8 && grade <= 9) {
            suffix = 'SMP';
        }
        $("#exam-grade").html(`<i class="fas fa-graduation-cap fa-fw"></i> ${grade} ${suffix}`);

        endTimer = Date.now()+(parseInt(examData.waktu_menit)*60000);
        offsetHour = new Date().getTimezoneOffset()/60;
    }
    setupExam();
    setExamQuestion(currentPos);
    // console.log(offsetHour)

    // timer
    let updateTimer = () => {
        let now = Date.now();
        let diff = endTimer - now;
        if (diff < 0) {
            diff = 0;
        }
        
        let timer = new Date(diff);
        let hours = timer.getHours()+offsetHour;
        let minutes = timer.getMinutes();
        let seconds = timer.getSeconds();
        
        let hoursStr = hours.toString().padStart(2, "0");
        let minutesStr = minutes.toString().padStart(2, "0");
        let secondsStr = seconds.toString().padStart(2, "0");

        $("#timer").html(`${hoursStr}:${minutesStr}:${secondsStr}`);

        return diff;
    }

    // submit data
    let checkData = () => {
        for (let i = 0; i < userAnswerData.length; i++) {
            if (userAnswerData[i].answerId == 0) {
                return false;
            }
        }

        return true;
    }

    let submit = (check) => {
        let con = true;
        if (check) {
            if (!checkData()) {
                con = confirm("Some questions have not been answered. Submit now?");
            }
        }

        if (con) {
            $("#submit-btn").css("display", "none");
            $("#fragment").scrollTop(0);
            $("#loader-make").css("display", "grid");

            $.ajax({
                type: 'post',
                url: '<?= base_url("/dashboard/submitanswer") ?>',
                data: {
                    answerData: userAnswerData,
                    id_soal : examData.id_soal
                },
                dataType: 'json',
                success: (data, textStatus, jqHXR) => {
                    console.log(data);
                    $("#loader-make").css("display", "none");

                    $("#result").html(Math.round(data.examResult.points * 100) / 100);
                    $("#falseAns").html(data.examResult.falseAnswer);
                    $("#correctAns").html(data.examResult.trueAnswer);
                    $("#exam-result").css("display", "block");

                    $(".exam-container").empty();
                    $(".exam-container").css("display", "none");
                    // beginTransaction('/'+control+'/getpage/exam');
                },
                error: (data, textStatus, jqHXR) => {
                    console.log(data);
                    $("#loader-make").css("display", "none");
                    // $("#loader-make").css("display", "none");
                    $("#submit-btn").css("display", "inherit");
                } 
            });
        }
    }

    // run timer
    let timerInterval = setInterval(() => {
        let res = updateTimer();
        if (res == 0) {
            clearInterval(timerInterval);
            submit(false);
        }
    }, 1000);


    // Interaction
    let showMap = false;
    let toggleShowMap = () => {
        showMap = !showMap;
        let display = showMap ? "grid" : "none";

        $("#exam-map").css("display", display);
    }

    // on bookmatk click
    $("#bookmark").click((event) => {
        toggleBookmark(currentPos);
        // console.log(userAnswerData);
    });

    // on go-to click
    $("#go-to").click((event) => {
        toggleShowMap();
    });

    // on #exam-map .number click
    $("#exam-map .number").click((event) => {
        currentPos = $("#exam-map .number").index(event.currentTarget);
        setExamQuestion(currentPos);
        toggleShowMap();
    });

    // on prev-btn click
    $("#prev-btn").click((event) => {
        if (currentPos > 0) {
            setExamQuestion(--currentPos);
        }
    });

    // on next-btn click
    $("#next-btn").click((event) => {
        if (currentPos < numQuest-1) {
            setExamQuestion(++currentPos);
        }
    });

    // on sidenav click
    $("#control-nav .nav-link").click((event) => {
        clearInterval(timerInterval);
    });

    // on submit-btn click
    $("#submit-btn").click((event) => {
        submit(true);
    });

    $(".exam-link").click((event) => {
        clickEvent(event, control, false);
    });
});
</script>
<h2 class="title">View Rank</h2>

Your Rank : <?= $rank ?>

<div class="elementary">
    <!-- <div class="title">Elementary School</div> -->

    <table id="rank-table" class="display">
        <thead>
            <tr>
                <th>#</th>
                <!-- <th>Student ID</th> -->
                <th>Name</th>
                <th>Grade</th>
                <th>School</th>
                <th>Points</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($rankData as $key => $value) :
            ?>
                <tr>
                    <td><?= $i++ ?></td>
                    <!-- <td><?= $value->id_siswa ?></td> -->
                    <td><?= $value->nama ?></td>
                    <td><?= $value->kelas ?></td>
                    <th><?= $value->asal_sekolah ?></th>
                    <td><?= round($value->total_poin, 2) ?></td>
                </tr>
            <?php
            endforeach;
            ?>
        </tbody>
    </table>
</div>

<script>
$("#rank-table").DataTable();
</script>